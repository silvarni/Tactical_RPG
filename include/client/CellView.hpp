
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* CellView.hpp */

#ifndef CELL_VIEW_HPP
#define CELL_VIEW_HPP

#include <SFML/System/Vector2.hpp>
#include <chrono>

#include "client/Animation.hpp"
#include "client/GraphicSystem.hpp"
#include "client/GraphicalEntity.hpp"
#include "client/OccupierView.hpp"
#include "common/Cell.hpp"


constexpr size_t CELL_WIDTH  = 32;
constexpr size_t CELL_HEIGHT = 32;

const sf::Vector2f CELL_SIZE = sf::Vector2f(CELL_WIDTH, CELL_HEIGHT);

class CellView
{
  public:
	CellView(void) = default;
	CellView(const Cell* cell, sf::Vector2f position, sf::Vector2f size,
			 GraphicSystem& graphic_system);

	// void update(float dt);

	void		reset(void);
	inline void toggle_select(void) { m_selected = !m_selected; }
	inline bool is_selected(void) const { return m_selected; }
	void		hovered(void);
	void		clicked(void);

	void draw(void);
	// void draw_occupier(GraphicSystem& graphic_system, bool selected =
	// false);

	inline const Cell* get_cell(void) { return mp_cell; }
	inline std::string get_texture_key(void) const { return m_texture_key; }
	inline Occupier*   get_occupier(void) { return mp_cell->m_occupier; }
	inline bool		   is_occupied(void) const
	{
		return mp_cell->m_occupier != nullptr;
	}
	inline int get_type(void) const
	{
		return (mp_cell == NULL) ? EMPTY : mp_cell->m_type;
	}

	virtual ~CellView(void);

	sf::Vertex		   verteces[4];
	sf::RectangleShape select[4];
	sf::Vector2f	   texCoords;

  protected:
	const Cell* mp_cell;
	bool		m_hovered	 = false;
	bool		m_clicked	 = false;
	bool		m_selected	= false;
	std::string m_texture_key = "cell_empty";
	Animation   m_animation;


  private:
};

#endif