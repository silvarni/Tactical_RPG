#ifndef ANIMATION_PLAYER_HPP
#define ANIMATION_PLAYER_HPP

#include "client/Animation.hpp"
#include "common/Occupier.hpp"

#include <unordered_map>

class AnimationPlayer
{
  public:
	AnimationPlayer(void);

	void add_animation(std::string key, size_t x, size_t y, size_t width,
					   size_t height, size_t nb_step, float step_time_s);

	bool is_playing(std::string key);
	bool is_ended(std::string key);
	bool has_animation(std::string key) const;
	void play(std::string key);
	inline std::string get_current_animation(void) const
	{
		return m_current_animation;
	}

	void update(GraphicalEntity& g, Occupier* o);
	inline bool is_active(void) const
	{
		return !has_animation(m_current_animation) ||
			   m_animations.at(m_current_animation).is_active();
	}
	inline void set_active(bool b)
	{
		m_animations[m_current_animation].set_active(b);
	}
	inline void set_default(std::string s) { m_default = s; }

	virtual ~AnimationPlayer(void);


  protected:
	std::unordered_map< std::string, Animation > m_animations;
	std::string m_current_animation = "_uninitialized";
	std::string m_default			= "idle";
};

#endif // ANIMATION_PLAYER_HPP