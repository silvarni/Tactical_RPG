#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include "client/settings.hpp"

#include "common/Occupier.hpp"
#include <memory>

class Globals
{
  public:
	static Globals& get(void);

	// Used for placing GUI elements on screen
	sf::Vector2u current_window_size =
		sf::Vector2u(WINDOW_WIDTH, WINDOW_HEIGHT);

	// Used for monitoring panel
	bool monitoring = false;

	// Used to disable scrolling
	bool has_focus = false;

	// Used to scroll with mouse
	bool mouse_scroll_enabled = false;

	// Used to auto-focus the camera on action
	bool auto_focus_enabled = false;

	// Used to position
	bool positioning = false;

	//
	bool can_check_finished = true;

	std::string name	  = "Anonymous";
	std::string room_name = "The Great War";

	// Our team is
	int team = -1;

	// Our gold
	int gold = 1000ul;

	// Turn Management
	bool  turn			 = false;
	int   turn_time		 = 20;
	float turn_time_left = 0;

	// Have we won ?
	bool win = false;

	virtual ~Globals(void);

  protected:
	Globals(void);
	static std::unique_ptr< Globals > m_self;
};

#endif // GLOBALS_HPP