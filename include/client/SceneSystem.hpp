#ifndef SCENE_SYSTEM_HPP
#define SCENE_SYSTEM_HPP
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* SceneSystem.hpp */

#include <memory>
#include <unordered_map>

#include "client/AudioSystem.hpp"
#include "client/GraphicSystem.hpp"


class Scene;

class SceneSystem
{
  public:
	explicit SceneSystem(GraphicSystem& graphic_system,
						 AudioSystem&   audio_system);

	void update(float dt);
	void display(void);

	void add_scene(std::string key, Scene* p_scene);

	void set_current_scene(const std::string key,
						   bool				 reinitialize_scene = true);

	virtual ~SceneSystem(void);

  protected:
	GraphicSystem& m_graphic_system;
	AudioSystem&   m_audio_system;
	Scene*		   m_current_scene = nullptr;
	std::unordered_map< std::string, std::shared_ptr< Scene > > m_scenes;

  private:
	SceneSystem(SceneSystem const& SceneSystem) = delete;
	SceneSystem& operator=(SceneSystem const& SceneSystem) = delete;
};

#endif // SCENE_SYSTEM_HPP