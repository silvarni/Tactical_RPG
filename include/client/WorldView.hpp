/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* WorldView.hpp */

#ifndef WORLD_VIEW_HPP
#define WORLD_VIEW_HPP

#include <SFML/Graphics/View.hpp>
#include <deque>
#include <memory>
#include <vector>

#include "client/AudioSystem.hpp"
#include "client/CellView.hpp"
#include "client/GraphicSystem.hpp"
#include "client/NetworkSystem.hpp"
#include "client/ViewManager.hpp"
#include "common/OccupierFactory.hpp"
#include "common/WorldData.hpp"


constexpr float HOVERED_TIME_S = 0.5;


class WorldView
{
  public:
	enum MODE
	{
		IDLE,
		SELECTED,
		TARGETING
	};

	WorldView(WorldData& world_data, GraphicSystem& graphic_system,
			  NetworkSystem& network_system, AudioSystem& audio_system);

	void reinitialize(void);
	void reinitialize_occupiers(void);
	void update(float dt);
	void		  update_controls(void);
	void		  display(void);
	void		  clear(void);
	OccupierView* find_occupier_view(const Occupier* occupier);
	void		new_turn(void);
	void		next_unit(void);
	std::string get_visual_name(size_t x, size_t y) const;
	void		print_current_actor_information(void);
	inline void add_spawn(sf::Vector2u		 pos) { m_spawns.push_back(pos); }
	inline std::vector< sf::Vector2u > const get_spawns(void) const
	{
		return m_spawns;
	}
	inline ViewManager& get_view_manager(void) { return m_view_manager; }
	inline void set_view_center(int x, int y)
	{
		m_view_manager.set_camera((float)x, (float)y);
	}
	virtual ~WorldView(void);

  protected:
	void select(Occupier* actor);
	void print_gui(void);
	void print_occupier_information(void);
	void draw_occupiers(void);
	void draw_cells(void);
	void add_line(sf::Vector2u a, sf::Vector2u b, sf::Color color);
	void add_cell_color(int x, int y, sf::Color color, bool degrade = false);
	CellView& get_cell(size_t x, size_t y);
	inline CellView& get_cell(sf::Vector2u pos)
	{
		return get_cell(pos.x, pos.y);
	}

	// Data & containment
	WorldData&												  m_world_data;
	NetworkSystem&											  m_network_system;
	std::vector< std::vector< std::shared_ptr< CellView > > > m_cells;
	std::vector< OccupierView >								  m_occupiers;
	sf::Vector2f											  m_focus;
	std::vector< sf::Vector2u >								  m_spawns;

	// Graphical
	ViewManager		m_view_manager;
	size_t			m_width;
	size_t			m_height;
	sf::VertexArray m_map;

	// Property
	std::chrono::time_point< std::chrono::steady_clock > m_hovered_time;

	// Access
	CellView*		m_clicked = nullptr;
	CellView*		m_hovered = nullptr;
	Occupier*		m_actor   = nullptr;
	Occupier*		m_target  = nullptr;
	GraphicalEntity m_graphical_hover;
	GraphicalEntity m_graphical_clicked;
	GraphicSystem&  m_graphic_system;
	AudioSystem&	m_audio_system;

	// Memory of last update
	double		 m_dt;
	sf::Vector2f m_view_last_position;
	sf::Vector2f m_view_last_size;
	sf::Vector2u m_last_cell_pos;
	bool		 m_is_anim = false;

	// Automaton
	MODE m_mode = IDLE;

	// Positioning
	OccupierFactory::TYPE m_positioning_type = OccupierFactory::TYPE::NONE;

  private:
};

#endif /// WORLD_VIEW_HPP