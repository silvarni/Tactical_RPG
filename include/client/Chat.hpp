/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Chat.hpp
 */

#ifndef CHAT_HPP
#define CHAT_HPP


#include <array>
#include <string>
#include <vector>


constexpr int CHAT_MAX_CHAR = 255;


class Query;


class Chat
{
  public:
	size_t MAX_CHAT_LINE_DISPLAYED = 6ul; // > 2

	explicit Chat(void);

	void update(Query query);
	void update(std::string message);
	Query display();

	virtual ~Chat(void);

  private:
	std::array< char, CHAT_MAX_CHAR > m_input;
	std::vector< std::string > m_messages;
	int						   m_chat_input_selected;
};


#endif // !CHAT_HPP