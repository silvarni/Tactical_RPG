
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* GraphicalEntity.hpp */

#ifndef GRAPHICAL_ENTITY_HPP
#define GRAPHICAL_ENTITY_HPP

#include <SFML/Graphics.hpp>

struct GraphicalEntity
{
	sf::Sprite  sprite;
	int			offset_x	= 0;
	int			offset_y	= 0;
	int			z_index		= 0ul;
	std::string texture_key = "error";
	bool		updated		= false;

	GraphicalEntity(void) = default;

	inline const sf::Sprite& get_sprite(void) const { return sprite; }

	GraphicalEntity(size_t x, size_t y, std::string texture_key,
					int z_index = 0ul)
		: z_index(z_index)
		, texture_key(texture_key)
	{
		sprite.setPosition(x, y);
	}

	bool operator<(const GraphicalEntity& g) const
	{
		if (z_index < g.z_index)
			return true;

		if (z_index > g.z_index)
			return false;

		if (sprite.getPosition().y < g.sprite.getPosition().y)
			return true;
		if (sprite.getPosition().y > g.sprite.getPosition().y)
			return false;

		return sprite.getPosition().x < g.sprite.getPosition().x;
	}
};

#endif // GRAPHICAL_ENTITY_HPP