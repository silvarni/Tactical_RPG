
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* GameScene.hpp */

#ifndef GAME_SCENE_HPP
#define GAME_SCENE_HPP

#include "client/AudioSystem.hpp"
#include "client/Chat.hpp"
#include "client/NetworkSystem.hpp"
#include "client/Scene.hpp"
#include "client/WorldView.hpp"
#include "common/WorldData.hpp"


class GameScene : public Scene
{
  public:
	explicit GameScene(NetworkSystem& network_system,
					   GraphicSystem& graphic_system,
					   AudioSystem&   audio_system);

	void update(float dt) override;
	void display(GraphicSystem& graphic_system,
				 AudioSystem&   audio_system) override;
	void enter_tree(void) override;
	void exit_tree(void) override;

	void set_enemi_name(Query query);
	void quit(void);

	virtual ~GameScene(void);

  protected:
	WorldData	  m_world;
	WorldView	  m_world_view;
	NetworkSystem& m_network_system;
	Chat		   m_chat;
	size_t		   m_updated			  = 0;
	bool		   m_positioning		  = true;
	bool		   m_checkbox_positioning = false;
	bool		   m_checkbox_endturn	 = false;

  private:
	GameScene(GameScene const& GameScene) = delete;
	GameScene& operator=(GameScene const& GameScene) = delete;
};

#endif