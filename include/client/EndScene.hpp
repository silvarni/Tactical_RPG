#include "client/Chat.hpp"
#include "client/NetworkSystem.hpp"
#include "client/Scene.hpp"

class EndScene : public Scene
{
  public:
	explicit EndScene(NetworkSystem& network_system);

	virtual void update(float dt);
	virtual void display(GraphicSystem& graphic_system,
						 AudioSystem&   audio_system);
	virtual void enter_tree(void);
	virtual void exit_tree(void);


	virtual ~EndScene(void);

  protected:
	NetworkSystem& m_network_system;
	Chat		   m_chat;
};