
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* OccupierView.hpp */

#ifndef OCCUPIER_VIEW_HPP
#define OCCUPIER_VIEW_HPP

#include <SFML/System/Vector2.hpp>

#include "client/AnimationPlayer.hpp"
#include "client/GraphicSystem.hpp"
#include "client/GraphicalEntity.hpp"
#include "common/Occupier.hpp"

struct Cell;

class OccupierView
{
  public:
	OccupierView(void) = default;
	OccupierView(Occupier* Occupier);

	void draw(GraphicSystem& graphic_system, const WorldData& world_data,
			  bool selected = false);

	inline bool		 is_occupied(void) const { return mp_occupier != nullptr; }
	inline Occupier* get_occupier(void) { return mp_occupier; }

	inline const GraphicalEntity& get_graphical_entity(void) const
	{
		return m_graphical_entity;
	}

	inline AnimationPlayer& get_animation_player(void)
	{
		return m_animation_player;
	}

	virtual ~OccupierView(void);


  protected:
	Occupier*		mp_occupier = nullptr;
	GraphicalEntity m_graphical_entity;
	bool			m_hovered = false;
	bool			m_clicked = false;
	AnimationPlayer m_animation_player;


  private:
};

#endif