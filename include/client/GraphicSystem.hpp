
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* GraphicSystem.hpp */

#ifndef GRAPHIC_SYSTEM_HPP
#define GRAPHIC_SYSTEM_HPP

#include <SFML/Graphics.hpp>
#include <set>

#include "client/GraphicalEntity.hpp"
#include "client/ResourceSystem.hpp"


#include "client/imgui-SFML.h"
#include "client/imgui/imgui.h"
#include "client/settings.hpp"

// > ImGui ShortCuts
constexpr ImGuiWindowFlags DEFAULT_WINDOW_FLAG =
	ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
	ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize;
constexpr std::size_t DEFAULT_WINDOW_WIDTH  = WINDOW_WIDTH / 3;
constexpr std::size_t DEFAULT_WINDOW_HEIGHT = WINDOW_HEIGHT / 3;
const sf::Vector2f	DEFAULT_WINDOW_SIZE =
	sf::Vector2f(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT);

// Buttons Default Parameters
const sf::Vector2f DEFAULT_BUTTON_SIZE =
	sf::Vector2f(WINDOW_WIDTH / 5, WINDOW_WIDTH / 30);
// > END ImGui

class GraphicSystem
{
  public:
	explicit GraphicSystem(ResourceSystem& resource_system);

	void update(float dt);
	void display(void);
	void clear(void);

	inline bool is_ended(void) const { return !m_window.isOpen(); }

	void add_entity(int x, int y, std::string texture_key = "NONE",
					int z_index = 0);
	void add_entity(const GraphicalEntity& entity);

	void add_to_vertex_array(sf::Vertex& v) { m_vertex_array.append(v); }
	void set_vertex_array(sf::VertexArray& map);
	void set_resize(void) { m_resize = m_vertex_array.getVertexCount(); }

	sf::Vector2f get_global_mouse_position(void) const;
	sf::Vector2i get_mouse_position(void) const;
	sf::Vector2f get_world_coordinates(sf::Vector2i v);
	ResourceSystem& get_resource_system(void);

	inline void set_view(sf::View view) { m_window.setView(view); }

	inline void			close(void) { m_window.close(); }
	inline sf::Vector2u get_window_size(void) const
	{
		return m_window.getSize();
	}
	void clear_contents(void);

	virtual ~GraphicSystem(void);

  protected:
	// From 30FPS with vector to 130FPS with set :D
	std::set< GraphicalEntity > m_entities;
	sf::RenderWindow			m_window;
	sf::VertexArray				m_vertex_array;
	ResourceSystem&				m_resource_system;
	sf::Vector2i				m_mouse_position;
	sf::Clock					m_clock; // ImGui
	size_t						m_resize = 0;

  private:
	GraphicSystem(GraphicSystem const& GraphicSystem) = delete;
	GraphicSystem& operator=(GraphicSystem const& GraphicSystem) = delete;
};

#endif // GRAPHICS_SYSTEM_HPP