#ifndef VIEW_MANAGER_HPP
#define VIEW_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include "client/CellView.hpp"
#include "client/Globals.hpp"
#include "client/GraphicSystem.hpp"
#include "client/settings.hpp"

constexpr int	MOUSE_MARGIN  = 50;
constexpr size_t MIN_VIEW_SIZE = CELL_WIDTH * 10;

class ViewManager
{
  public:
	explicit ViewManager(size_t width, size_t height,
						 GraphicSystem& graphic_system);

	void update(float dt, sf::Vector2f focus);
	void display(void);
	void check_bounds(void);
	void zoom(float delta);
	void set_world_size(sf::Vector2f size);
	void new_turn(void);
	void set_auto_focus(bool b) { m_auto_focus = b; }
	inline void set_camera(int x, int y)
	{
		m_view.setCenter((float)x, (float)y);
	}

	virtual ~ViewManager();

  protected:
	bool update_auto(float dt);
	size_t		   m_width  = WINDOW_WIDTH;
	size_t		   m_height = WINDOW_HEIGHT;
	sf::View	   m_view;
	GraphicSystem& m_graphic_system;
	double		   m_speed_view = CELL_WIDTH * 20;
	double		   m_dt;
	size_t		   m_world_width;
	size_t		   m_world_height;
	sf::Vector2i   m_last_mouse_position;
	bool		   m_auto_focus = false;
};


#endif // VIEW_MANAGER_HPP