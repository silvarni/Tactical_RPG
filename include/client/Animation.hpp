#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <SFML/Graphics.hpp>

#include "client/GraphicalEntity.hpp"
#include <chrono>

class Animation
{
  public:
	Animation(void) = default;
	Animation(size_t x, size_t y, size_t nb_step, float step_time_s = 0.3f,
			  size_t width  = 32,
			  size_t height = 32); // hard code

	void update(GraphicalEntity& g);
	void update(sf::Vertex* v0, sf::Vector2f texCoords);

	inline void set_repeat(bool b) { m_repeat = b; }
	inline void set_autorun(bool b) { m_autorun = b; }
	void set_active(bool b);
	inline bool is_active(void) { return m_active; }
	inline void set_nb_steps(size_t n) { m_steps = n; }
	inline size_t					get_step(void)
	{
		return (std::chrono::steady_clock::now() - m_start).count() /
			   1000000000.f;
	}
	inline bool is_active(void) const { return m_active; }

	inline bool is_ended(void) const { return m_ended; }

	virtual ~Animation(void);

  protected:
	size_t x;
	size_t y;
	bool   m_animated	= false;
	size_t m_steps		 = 0ul;
	float  m_step_time_s = 1;
	size_t m_width;
	size_t m_height;
	bool   m_repeat  = false;
	bool   m_autorun = false;
	bool   m_active  = false;
	bool   m_ended   = false;

	std::chrono::time_point< std::chrono::steady_clock > m_start;
};

#endif // ANIMATION_HPP