/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* INPUT */

#ifndef INPUT_HPP
#define INPUT_HPP

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <memory>
#include <unordered_map>
struct Control
{
	bool pressed		   = false;
	bool is_toggle_control = false;
	operator bool() const { return pressed; }
	void operator=(bool b)
	{
		if (is_toggle_control)
		{
			pressed = !pressed;
		}
		else
		{
			pressed = b;
		}
	}
};

class Input
{
  public:
	void initialize(void);

	static void update(sf::Event event);
	static Control& is_action_pressed(std::string key);
	static bool is_any_key_pressed(void);
	virtual ~Input(void);

  protected:
	Input(void);


	static std::unordered_map< std::string, Control > m_inputs;
	static std::unique_ptr< Input > m_input;

  private:
};

#endif