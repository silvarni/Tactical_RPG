/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file NetworkSystem.hpp
 * \author Romain Mekarni
 */

#ifndef NETWORK_SYSTEM_HPP
#define NETWORK_SYSTEM_HPP

#include <SFML/Network.hpp>
#include <atomic>
#include <string>
#include <thread>


#include "common/Query.hpp"
#include "common/settings.hpp"

constexpr int MAX_TRY	  = 5;
constexpr int TIME_RETRY_S = 1;


class NetworkSystem
{
  public:
	explicit NetworkSystem(void);

	void initialize(const char* ip_address, int port);
	void connect(void);
	void disconnect(void);
	void receive(void);
	void send(Query query);
	void send(sf::Int8 query);
	void update(float dt);
	inline void interrupt(void) { m_running.clear(); }
	inline bool is_connecting(void) { return m_connecting.test_and_set(); }
	inline bool is_connected(void) { return m_connected.test_and_set(); }
	inline bool is_running(void) {return m_running.test_and_set();}
	Query		get_received();

	virtual ~NetworkSystem(void);

  protected:
	sf::TcpSocket	m_socket;
	std::thread		 m_thread;
	Query			 m_received;
	std::atomic_flag m_running	= ATOMIC_FLAG_INIT,
					 m_receiving  = ATOMIC_FLAG_INIT,
					 m_connecting = ATOMIC_FLAG_INIT,
					 m_connected  = ATOMIC_FLAG_INIT;
	const char* m_ip_address	  = nullptr;
	size_t		m_port;
};

#endif
