/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*LobbyScene.hpp */


#ifndef LOBBY_SCENE_HPP
#define LOBBY_SCENE_HPP


#include "client/Chat.hpp"
#include "client/Scene.hpp"


class NetworkSystem;


class LobbyScene : public Scene
{
  public:
	explicit LobbyScene(NetworkSystem& network_system);

	void update(float dt) override;
	void display(GraphicSystem& graphic_system, AudioSystem& audio_system) override;
	void quit(void);
	void enter_tree(void) override;
	void exit_tree(void) override;

	virtual ~LobbyScene(void);

  protected:
	void draw_room_list(void);
	void draw_room_creation_menu(void);
	void draw_player_list(void);


	NetworkSystem& m_network_system;
	bool		   m_ready;

  private:
	std::vector< char* > mv_rooms;
	std::vector< char* > mv_players;
	Chat				 m_chat;
	int					 selected_room   = 0;
	int					 selected_player = 0;
	int					 nb_teams		 = 2;
	int					 nb_players		 = nb_teams;
};

#endif // LOBBY_SCENE_HPP