
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* GraphicSystem.hpp */

#ifndef RESOURCE_SYSTEM_HPP
#define RESOURCE_SYSTEM_HPP

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include <unordered_map>


const std::string ASSET_DIR = "assets/";

class ResourceSystem
{
  public:
	explicit ResourceSystem(void);

	// -- Texture
	// A = 0, B = 1, C = 2, D = 3
	sf::Vector2f get_texture_coordinates(std::string key, size_t index);
	sf::Texture* get_texture(std::string key);

	// -- Sounds
	sf::SoundBuffer* get_sound(std::string key);

	// -- Musics
	sf::Music* get_music(std::string key);


	virtual ~ResourceSystem(void);

  protected:
	bool load_texture(std::string key);
	bool load_music(std::string& key);
	bool load_sound(std::string key);

	void add_texture_coordinates(std::string key, sf::Vector2f pos,
								 sf::Vector2f size);

	// Textures
	std::unordered_map< std::string, sf::Texture >  m_textures;
	std::unordered_map< std::string, sf::Vector2f > m_texture_coordinates;

	// Musics
	std::unordered_map< std::string, sf::Music > m_musics;

	// Sounds
	std::unordered_map< std::string, sf::SoundBuffer > m_sound_buffers;

  private:
	ResourceSystem(ResourceSystem const& ResourceSystem) = delete;
	ResourceSystem& operator=(ResourceSystem const& ResourceSystem) = delete;
};

#endif // RESOURCE_SYSTEM_HPP