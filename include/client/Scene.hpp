#ifndef SCENE_HPP
#define SCENE_HPP
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Scene.hpp */

#include <memory>

#include "client/AudioSystem.hpp"
#include "client/GraphicSystem.hpp"


class Scene
{
  public:
	explicit Scene(std::string next_scene);


	virtual void update(float dt) = 0;
	virtual void display(GraphicSystem& graphic_system,
						 AudioSystem&   audio_system) = 0;
	virtual void enter_tree(void)					  = 0;
	virtual void exit_tree(void)					  = 0;

	inline void set_active(bool b) { m_active = b; }

	inline bool is_active(void) const { return m_active; }

	inline std::string get_next_scene(void) const { return m_next_scene; }

	virtual ~Scene(void);

  protected:
	bool		m_active	 = false;
	std::string m_next_scene = "UNSET";
};

#endif
