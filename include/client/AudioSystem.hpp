#ifndef AUDIO_SYSTEM_HPP
#define AUDIO_SYSTEM_HPP

#include <SFML/Audio.hpp>
#include <vector>

#include "client/ResourceSystem.hpp"

class AudioSystem
{
  public:
	explicit AudioSystem(ResourceSystem& resource_system);

	void play_music(std::string key, bool loop = true);
	void play_sound(std::string  key,
					sf::Vector2f position = sf::Vector2f(0, 0));
	void clear(void);
	void pause(void);

	virtual ~AudioSystem(void);


  protected:
	ResourceSystem& m_resource_system;
	sf::Music*		m_current_music = nullptr;
	std::string		m_current_music_key;

	bool					 m_is_playing = false;
	std::vector< sf::Sound > m_sounds;
};

#endif // AUDIO_SYSTEM_HPP