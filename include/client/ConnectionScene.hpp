/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*ConnectionScene.hpp */
#ifndef CONNECTION_SCENE_HPP
#define CONNECTION_SCENE_HPP

#include "client/NetworkSystem.hpp"
#include "client/Scene.hpp"

class ConnectionScene : public Scene
{
  public:
	explicit ConnectionScene(NetworkSystem& network_system);

	void update(float dt) override;
	void display(GraphicSystem& graphic_system, AudioSystem& audio_system) override;
	void enter_tree(void) override;
	void exit_tree(void) override;

	virtual ~ConnectionScene(void);

  protected:
	NetworkSystem& m_network_system;
	bool		   m_connecting			  = false;
	bool		   m_connected			  = false;
	bool		   m_has_tried_connection = false;
	char		   m_ip_address[255]	  = "127.0.0.1";
	int m_port = 53000;
};

#endif // CONNECTION_SCENE_HPP