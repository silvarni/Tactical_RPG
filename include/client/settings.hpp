#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <SFML/System/Vector2.hpp>
#include <iostream>
#include <string>


constexpr size_t WINDOW_WIDTH  = 1024ul;
constexpr size_t WINDOW_HEIGHT = 768ul;

constexpr size_t VIEW_SIZE = 32 * 16;

constexpr char WINDOW_TITLE[] = "Tactical RPG";

#endif