#ifndef SPLASHSCREEN_SCENE_HPP
#define SPLASHSCREEN_SCENE_HPP

/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "client/Scene.hpp"

#include <memory>
class GraphicSystem;

class SplashScreenScene : public Scene
{
  public:
	explicit SplashScreenScene(void);


	void update(float dt) override;
	void display(GraphicSystem& graphic_system, AudioSystem& audio_system) override;
	void enter_tree(void) override;
	void exit_tree(void) override;

	virtual ~SplashScreenScene(void);

  protected:
	sf::View m_view;

  private:
};


#endif // SPLASHSCREEN_SCENE_HPP