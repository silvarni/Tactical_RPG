
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Core.hpp */

#ifndef CORE_HPP
#define CORE_HPP

#include "client/AudioSystem.hpp"
#include "client/GraphicSystem.hpp"
#include "client/NetworkSystem.hpp"
#include "client/ResourceSystem.hpp"
#include "client/SceneSystem.hpp"
#include "common/Query.hpp"

class Core
{
  public:
	explicit Core(void);
	void run(void);

	virtual ~Core(void);

  protected:
	GraphicSystem  m_graphic_system;
	SceneSystem	m_scene_system;
	ResourceSystem m_resource_system;
	NetworkSystem  m_network_system;
	AudioSystem	m_audio_system;
	bool		   m_running = true;

  private:
	Core(Core const& Core) = delete;
	Core& operator=(Core const& Core) = delete;
};

#endif // CORE_HPP