/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Query.hpp
 * \author Romain Mekarni
 */

#ifndef QUERY_HPP
#define QUERY_HPP

#include <SFML/Network.hpp>
#include <type_traits>

#include "common/Cell.hpp"
// #include "common/Occupier.hpp"
// #include "common/WorldData.hpp"
class WorldData;
class Occupier;


enum
{
	WORLD = 1,
	TURN,
	MOVE,
	ATTACK,
	NAME,
	CHAT,
	NOTHING,
	DISCONNECT,
	READY,
	JOIN,
	FULL,
	CLIENTS,
	ROOMS,
	ROOM,
	CREATE,
	UPDATE,
	START,
	BACK,
	TEAM,
	TEAMS,
	POSITIONING,
	SPAWNS,
	INIT_WORLD,
	CREATE_OCCUPIER,
	GOLD,
	END,
};


template < typename T > sf::Packet& operator<<(sf::Packet& packet, const T& in)
{
	static_assert(std::is_convertible< T, sf::Int8 >::value);
	return packet << static_cast< sf::Int8 >(in);
}

template < typename T > sf::Packet& operator>>(sf::Packet& packet, T& in)
{
	sf::Int8 out;
	packet >> out;
	in = static_cast< T >(out);
	return packet;
}

sf::Packet& operator<<(sf::Packet& packet, const sf::Vector2u in);
sf::Packet& operator>>(sf::Packet& packet, sf::Vector2u& in);

sf::Packet& operator<<(sf::Packet& packet, const Cell& cell);
bool operator>>(sf::Packet& packet, Cell& cell);

sf::Packet& operator<<(sf::Packet& packet, const Occupier& occupier);
sf::Packet& operator>>(sf::Packet& packet, Occupier& occupier);

sf::Packet& operator<<(sf::Packet& packet, const WorldData& world);
sf::Packet& operator>>(sf::Packet& packet, WorldData& world);


class Query : public sf::Packet
{
  public:
	explicit Query(void);

	virtual ~Query(void);

	static std::string log(sf::Int8 action);
};


#endif
