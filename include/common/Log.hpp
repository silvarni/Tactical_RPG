
/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Log.hpp */

#ifndef LOG_HPP
#define LOG_HPP


#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector2.hpp>

enum SEVERITY
{
	LOW,
	MEDIUM,
	HIGHT,
	CRITICAL,
	ERROR,
	NONE
};

class Log
{
  public:
	static void initialize(std::string output_name);


	static void write(std::string message, SEVERITY severity = SEVERITY::NONE);
	static void write_error(std::string message);
	static void write_debug(std::string message);
	static void destroy(void);

	// Templates
	template < typename T > static std::string to_string(sf::Rect< T > rect)
	{
		return Log::to_string(sf::Vector2u(rect.left, rect.top)) +
			   Log::to_string(sf::Vector2u(rect.width, rect.height));
	}

	template < typename T > static std::string to_string(sf::Vector2< T > pos)
	{
		return std::string("(" + std::to_string(pos.x) + "," +
						   std::to_string(pos.y) + ")");
	}


	virtual ~Log(void);

  protected:
	explicit Log(void);

	static std::unique_ptr< Log >								m_self;
	static std::ofstream										m_output;
	static std::string											m_target;
	static std::chrono::time_point< std::chrono::steady_clock > m_start;

	// everything below MEDIUM will be ignored
	const static SEVERITY m_level = SEVERITY::LOW;


  private:
};


#endif
