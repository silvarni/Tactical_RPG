/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef OCCUPIER_HPP
#define OCCUPIER_HPP

#include <SFML/System/Vector2.hpp>
#include <chrono>
#include <climits>
#include <deque>
#include <queue>
#include <string>

#include "common/Cell.hpp"
#include "common/Query.hpp"

constexpr float TIME_TO_TRAVEL_ONE_CELL_S = 0.3f;

enum DIRECTION
{
	NORTH = 1,
	EAST,
	WEST,
	SOUTH
};

enum ACTION
{
	IDLE,
	MOVING,
	ATTACKED,
	ATTACKING,
	MOVE_AND_ATTACK,
	DIE,
};

class WorldData;

using astar_node_t			= std::pair< const Cell*, int >;
static auto astar_compare_f = [](const astar_node_t& p1,
								 const astar_node_t& p2) {
	return p1.second > p2.second;
};

using astar_queue_t =
	std::priority_queue< astar_node_t, std::vector< astar_node_t >,
						 decltype(astar_compare_f) >;


class Occupier
{
  public:
	explicit Occupier(sf::Vector2u pos, std::string name = "Unknow Unit",
					  std::string weapon_name = "knife", int team = -1,
					  float damage = 0.f, float max_health = 100.f,
					  int speed = 5, int range = 1,
					  DIRECTION direction = SOUTH);

	// Getter
	inline bool  is_alive(void) const { return (m_health > 0.f); }
	inline float get_damage(void) const { return m_damage; }
	inline float get_health(void) const { return m_health; }
	inline float get_max_health(void) const { return m_max_health; }
	inline int   get_speed(void) const { return m_speed; }
	inline int   get_range(void) const { return m_range; }
	inline int   get_movement_points(void) const { return m_speed - m_steps; }
	inline int   get_direction(void) const { return m_direction; }
	inline std::string  get_name(void) const { return m_name; }
	inline int			get_team(void) const { return m_team; }
	inline sf::Vector2u get_position(void) const { return m_position; }
	inline sf::Vector2u get_target(void) const { return m_target; }
	inline int			get_action(void) const { return m_action; }
	inline float		get_action_time(void) const { return m_action_time; }
	inline bool			has_finished(void) const
	{
		return get_movement_points() == 0 && m_has_attacked;
	}

	inline void set_action(ACTION action) { m_action = action; }
	inline void set_team(int team) { m_team = team; }

	void update(float dt, WorldData& world);
	void update(WorldData& world);
	void update_range(const WorldData& world, int max = INT_MAX);

	friend sf::Packet& operator<<(sf::Packet& packet, const Occupier& occupier);
	friend sf::Packet& operator>>(sf::Packet& packet, Occupier& occupier);

	bool go_to(size_t x, size_t y, WorldData& world,
			   bool to_occupied_cell = false);

	void attack(Occupier* enemy);
	void suffer(float damage);
	bool can_walk_on(TYPE type);
	void new_turn(void);

	std::string get_str_direction(void) const;

	std::deque< DIRECTION >	path;
	std::vector< const Cell* > move_range;
	std::vector< const Cell* > attack_range;

	inline bool is_moving(void) const
	{
		return m_action == ACTION::MOVING ||
			   m_action == ACTION::MOVE_AND_ATTACK;
	}
	inline void set_positioning(bool b) { m_positioning = b; }
	inline void set_weapon_name(std::string w_name) { m_weapon_name = w_name; }
	inline std::string get_weapon_name(void) const { return m_weapon_name; }

	~Occupier(void);

  protected:
	sf::Vector2u m_position;
	sf::Vector2u m_target;
	std::string  m_name		   = "Unknow Unit";
	std::string  m_weapon_name = "knife";
	int			 m_team;
	float		 m_damage		= 0.f;
	float		 m_max_health   = 1.f;
	float		 m_health		= 1.f;
	int			 m_speed		= 1;
	int			 m_range		= 1;
	int			 m_steps		= 0;
	float		 m_action_time  = 0.f;
	bool		 m_positioning  = true;
	bool		 m_has_attacked = false;

	DIRECTION m_direction = SOUTH;
	ACTION	m_action	= IDLE;
};


#endif
