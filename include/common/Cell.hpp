/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Cell.hpp
 * \author Romain Mekarni
 */

#ifndef CELL_HPP
#define CELL_HPP

#include <SFML/System/Vector2.hpp>
#include <memory>

#include "common/Log.hpp"

class Occupier;

enum TYPE
{
	EMPTY = 0,
	ROCK,
	WATER,
	TREE,
	HOUSE
};

struct Cell
{
	explicit Cell(TYPE type = EMPTY);
	explicit Cell(size_t x, size_t y, TYPE type = EMPTY);
	std::string			get_visual_name(void) const;
	inline sf::Vector2u get_pos() const { return sf::Vector2u(x, y); }

	size_t	x;
	size_t	y;
	TYPE	  m_type;
	Occupier* m_occupier = nullptr;
	bool	  m_updated  = false;
};

#endif
