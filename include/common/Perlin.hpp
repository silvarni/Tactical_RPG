#ifndef PERLIN_HPP
#define PERLIN_HPP

#include <iostream> // allows size_t
#include <vector>


class Perlin
{
  public:
	Perlin(size_t w, size_t h, size_t step, bool autoRandom = true);

	void display(void);
	void reset(void);

	/* =============== GETTER =============== */
	const double get(size_t x, size_t y) const;
	const size_t getWidth(void) const;
	const size_t getHeight(void) const;

	virtual ~Perlin();

  protected:
	void initTab(void);
	void interpolate(void);
	bool inMap(size_t i, size_t j) const;
	size_t random(size_t min, size_t max) const;
	void   randomize(void);
	double linear_interpolation(double a, double b, double t) const;
	double polynomial_interpolation(double a, double b, double t) const;

	size_t								 m_step;
	size_t								 m_width;
	size_t								 m_height;
	std::vector< std::vector< double > > m_tab;
};

#endif // PERLIN_HPP
