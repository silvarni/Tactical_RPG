/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef OCCUPIER_FACTORY_HPP
#define OCCUPIER_FACTORY_HPP


#include <SFML/Graphics/View.hpp>
#include <string>

class Occupier;


class OccupierFactory
{
  public:
	enum TYPE : int
	{
		NONE,
		SOLDIER,
		ELF,
		ORC,
		SCOUT,
		MAX
	};

	OccupierFactory()  = default;
	~OccupierFactory() = default;

	static Occupier* create_unit(sf::Vector2u pos, int team,
								 OccupierFactory::TYPE type);
	static int get_cost(OccupierFactory::TYPE type);

	static std::string get_unit_name(OccupierFactory::TYPE type);

  private:
};


#endif // !OCCUPIER_FACTORY_HPP
