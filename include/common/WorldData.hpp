/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>. */

/**
 * \file WorldData.hpp
 * \author Romain Mekarni
 */

#ifndef WORLDDATA_HPP
#define WORLDDATA_HPP


#include "common/Cell.hpp"
#include "common/Occupier.hpp"
#include "common/Perlin.hpp"
#include <deque>
#include <vector>


constexpr size_t WORLD_WIDTH  = 64;
constexpr size_t WORLD_HEIGHT = 36;

struct Action
{
	sf::Vector2u position;
	ACTION		 action;
	Occupier*	actor;

	Action(ACTION a, sf::Vector2u pos, Occupier* o)
		: position(pos)
		, action(a)
		, actor(o){};
};

class Query;

class WorldData
{
  public:
	static int distance(sf::Vector2u x, sf::Vector2u y);

	explicit WorldData(void);

	void reinitialize();
	void make_perlin(const Perlin& perlin);
	Query update(Query in);
	void refresh(Query in);
	void update(float dt);
	void update(void);
	void add_occupier(Occupier*);
	void new_turn(void);

	Query get_query(void);

	const Cell& get_cell(size_t x, size_t y) const;
	inline const Cell& get_cell(sf::Vector2u pos) const
	{
		return m_cells[pos.y][pos.x];
	}
	Cell& get_cell(size_t x, size_t y);
	inline Cell& get_cell(sf::Vector2u pos) { return get_cell(pos.x, pos.y); }
	inline const Occupier* get_occupier(sf::Vector2u pos) const
	{
		return get_cell(pos).m_occupier;
	}
	Occupier* get_occupier(sf::Vector2u pos)
	{
		return get_cell(pos).m_occupier;
	}
	inline void set_occupier(sf::Vector2u pos, Occupier* occupier)
	{
		get_cell(pos).m_occupier = occupier;
	}
	void remove_occupier(Occupier* occupier);
	inline std::vector< std::shared_ptr< Occupier > >& get_occupiers()
	{
		return m_occupiers;
	}

	std::vector< const Cell* > get_neighboor_cells(size_t x, size_t y) const;
	void set_cell(size_t x, size_t y, TYPE type);

	int has_empty_team(size_t nb_team) const;

	virtual ~WorldData(void);

	inline bool is_updated(void)
	{
		bool res  = m_updated;
		m_updated = false;
		return res;
	}

	std::deque< Action > actions;

  protected:
	bool m_updated = true;
	Cell m_cells[WORLD_HEIGHT][WORLD_WIDTH];
	std::vector< std::shared_ptr< Occupier > > m_occupiers;
};

#endif
