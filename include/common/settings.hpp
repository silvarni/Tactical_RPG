#ifndef SETTINGS_H
#define SETTINGS_H


#include <SFML/Network.hpp>
#include <chrono>

constexpr std::chrono::duration< double > FRAMERATE(1.f / 60.f);


#endif
