/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Client.hpp
 */

#ifndef CLIENT_HPP
#define CLIENT_HPP


#include <SFML/Network.hpp>

#include "common/Query.hpp"
#include "server/Room.hpp"
#include "server/Team.hpp"


class Client
{
  public:
	explicit Client() = default;

	Client(const Client&) = delete;
	Client(Client&&)	  = delete;
	Client& operator=(const Client&) = delete;
	Client& operator=(Client&&) = delete;

	void initialize(std::string name);
	void send(Query query);
	void send(sf::Int8 query);
	void send(std::string message);

	inline std::string	get_name() const { return m_name; }
	inline Team*		  get_team() const { return m_team; }
	inline Room*		  get_room() const { return m_room; }
	inline sf::TcpSocket& get_socket() { return m_socket; }
	inline int 		  get_gold() const {return m_gold;}
	inline bool			  is_ready() const { return m_ready; }
	inline bool			  is_positioned() const { return m_positioned; }

	inline void set_name(std::string name) { m_name = name; }
	inline void set_team(Team* team) { m_team = team; }
	inline void set_room(Room* room) { m_room = room; }
	inline void set_ready(bool value) { m_ready = value; }
	inline void set_gold(size_t gold) {m_gold = gold;}
	inline void set_positioned(bool value) { m_positioned = value; }
	inline void toggle_ready() { m_ready = (m_ready) ? false : true; }
	inline void toggle_positioned()
	{
		m_positioned = (m_positioned) ? false : true;
	}
	inline void pay(size_t price){m_gold -= price;}

	virtual ~Client(void);

  private:
	std::string   m_name;
	Team*		  m_team = nullptr;
	Room*		  m_room = nullptr;
	sf::TcpSocket m_socket;
	bool		  m_ready	  = false;
	bool		  m_positioned = false;
	int 	m_gold = 0ul;
};


#endif // !CLIENT_HPP