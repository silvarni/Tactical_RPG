/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Core.hpp
 */

#ifndef CORE_HPP
#define CORE_HPP


#include <SFML/Network.hpp>
#include <atomic>
#include <csignal>
#include <cstdlib>
#include <thread>
#include <vector>

#include "common/settings.hpp"
#include "server/Lobby.hpp"
#include "server/Network.hpp"


class Client;
class Query;


class Core : public Network
{
  public:
	explicit Core(size_t port);

	static void SIGINT_handler(int);
	void		receive(void);
	void update(Query query, Client* client) override;
	void remove_client(Client* client);

	virtual ~Core(void);

	static sig_atomic_t m_running;

  private:
	size_t m_port;
	void (*hand_SIGINT)(int);
	sf::SocketSelector									 m_selector;
	sf::TcpListener										 m_listener;
	Lobby												 m_lobby;
	std::chrono::time_point< std::chrono::steady_clock > m_last;
	float												 m_next_turn = 0;
};


#endif // !CORE_HPP
