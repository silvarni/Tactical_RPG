/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Network.hpp
 */


#ifndef NETWORK_HPP
#define NETWORK_HPP


#include <vector>

#include "common/Query.hpp"


class Client;


class Network
{
  public:
	explicit Network(void) = default;

	virtual void update(Query query, Client* client) = 0;

	void add_client(Client* client);
	void remove_client(Client* client);

	void broadcast(Query query) const;
	void broadcast(sf::Int8 in) const;
	void broadcast(std::string message) const;
	Query dump_clients(void) const;

	inline int get_nb_clients() const { return (int)mvp_clients.size(); }

	virtual ~Network(void) = default;

  protected:
	std::vector< Client* > mvp_clients;
};


#endif // !NETWORK_HPP