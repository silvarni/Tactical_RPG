/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Client.hpp
 */

#ifndef ROOM_HPP
#define ROOM_HPP


#include <SFML/Network.hpp>
#include <chrono>
#include <string>

#include "common/WorldData.hpp"
#include "server/Network.hpp"
#include "server/Team.hpp"

class Client;
class Query;


class Room : public Network
{
  public:
	explicit Room(std::string name, int nb_teams, int nb_players, int turn_time,
				  int gold);

	void reinitialize_game(void);
	void initialize_map(void);
	void initialize_game(void);
	void start_game(void);

	float update(float dt);
	void update(Query query, Client* client) override;
	void add_client(Client* client);
	void remove_client(Client* client);
	Query turn(int turn_time);
	Query dump_teams(void) const;
	void add_spawn_zone(int team, sf::Vector2u pos1, sf::Vector2u pos2);

	inline std::string get_name() const { return m_name; }
	inline int		   get_nb_players() const { return m_nb_players; }
	inline int		   get_nb_teams() const { return (int)mv_teams.size(); }
	inline int		   get_turn_time() const { return (int)m_turn_time; }
	inline bool		   is_game_running() const { return m_game_running; }
	inline bool		   full() const
	{
		return (mvp_clients.size() == (size_t)m_nb_players);
	}

	virtual ~Room(void) = default;

  private:
	std::string								   m_name;
	int										   m_nb_players;
	size_t									   m_current_team;
	std::vector< Team >						   mv_teams;
	WorldData								   m_world;
	Perlin									   m_perlin;
	std::vector< std::vector< sf::Vector2u > > m_spawns;
	float									   m_turn_time;
	float									   m_positioning_time = 60.f;
	float									   m_delay			  = 2.f;
	float									   m_elapsed_time;
	bool									   m_game_running = false;
	bool									   m_positioning  = false;
	int										   m_initial_gold;
	bool									   m_pause = false;
};


#endif // !ROOM_HPP