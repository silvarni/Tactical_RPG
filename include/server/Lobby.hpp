/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Team.hpp
 */

#ifndef LOBBY_HPP
#define LOBBY_HPP


#include "server/Network.hpp"
#include "server/Room.hpp"


class Lobby : public Network
{
  public:
	explicit Lobby(void) = default;

	void add_client(Client* client);
	void remove_client(Client* client);
	void broadcast(Query query) const;
	void if_empty_then_remove(Room* room);
	float update(float dt);
	void update(Query query, Client* client);
	Query dump_rooms(void) const;

	virtual ~Lobby(void) = default;

  private:
	std::vector< std::unique_ptr< Room > > mvu_rooms;
};


#endif // !LOBBY_HPP