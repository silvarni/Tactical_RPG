/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file Team.hpp
 */

#ifndef TEAM_HPP
#define TEAM_HPP


#include <string>
#include <vector>

#include "server/Network.hpp"


class Client;
class Query;


class Team : public Network
{
  public:
	explicit Team(std::string name);

	void initialize(void);
	void update(Query query, Client* client) override;
	inline const std::vector< Client* >& get_clients(void) const
	{
		return mvp_clients;
	}
	inline std::vector< Client* >& get_clients(void)
	{
		return mvp_clients;
	}
	inline const std::string get_name(void) const { return m_name; }
	inline Client*			 get_current_player(void) const
	{
		return mvp_clients[m_current_player];
	}
	void next_player(void);

	virtual ~Team(void) = default;

  private:
	std::string m_name;
	size_t		m_current_player = 0;
};


#endif // !TEAM_HPP