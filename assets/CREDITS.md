# Credits
### Sprisheets
* Thanks to __Universal LPC Character__ : http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/#?body=orc_red
* House from http://opengameart.org/content/seven-kingdoms (7 Kingdoms : The game)
* Icons :
    * Most http://opengameart.org/content/flare-item-variation-60x60-only
    * Coin : http://opengameart.org/content/gold-cointoken
    * Heart : http://opengameart.org/content/heart-3
### Sounds 
* Door sounds : Iwan 'qubodup' Gabovitch <qubodup@gmail.com> 

### Music 
* Intro : found http://opengameart.org/content/tavern-0
* War : http://opengameart.org/content/solemn-war-music