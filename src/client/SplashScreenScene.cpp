#include "client/SplashScreenScene.hpp"

#include "client/Globals.hpp"
#include "client/settings.hpp"
#include "imgui.h"

SplashScreenScene::SplashScreenScene(void)
	: Scene("game")
	, m_view(sf::Vector2f(1024 / 2, 768 / 2), sf::Vector2f(1024, 768))
{
}

void SplashScreenScene::update(float dt)
{
}

void SplashScreenScene::display(GraphicSystem& graphic_system,
								AudioSystem&   audio_system)
{
	audio_system.play_music("intro");
	graphic_system.set_view(m_view);

	// GUI
	auto window_size = Globals::get().current_window_size;
	ImGui::SetNextWindowPos(
		sf::Vector2f(0.4 * window_size.x, 0.4 * window_size.y));
	if (ImGui::Begin("Main Menu", nullptr,
					 DEFAULT_WINDOW_FLAG | ImGuiWindowFlags_NoTitleBar))
	{
		// style
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
							  ImVec4(255, 255, 255, 0.6));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive,
							  ImVec4(255, 255, 255, 0.8));
		ImGui::SetWindowSize(DEFAULT_WINDOW_SIZE);


		// Images
		sf::Sprite play;
		play.setTexture(
			*graphic_system.get_resource_system().get_texture("button_play"));
		sf::Sprite exit_b;
		exit_b.setTexture(
			*graphic_system.get_resource_system().get_texture("button_exit"));


		if (ImGui::ImageButton(exit_b))
		{
			m_active	 = false;
			m_next_scene = "exit";
		}
		ImGui::SameLine();
		if (ImGui::ImageButton(play))
		{
			m_next_scene = "connection";
			m_active	 = false;
		}
		ImGui::PopStyleColor(3);
		ImGui::End();
	}

	// SFML
	graphic_system.add_entity(0, 0, "splashscreen_background");
}

void SplashScreenScene::enter_tree(void)
{
}

void SplashScreenScene::exit_tree(void)
{
}

SplashScreenScene::~SplashScreenScene(void)
{
}