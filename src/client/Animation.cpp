#include "Animation.hpp"
#include "common/Log.hpp"

#include <cassert>

Animation::Animation(size_t x, size_t y, size_t nb_step, float step_time_s,
					 size_t width, size_t height)
	: x(x)
	, y(y)
	, m_steps(nb_step)
	, m_step_time_s(step_time_s)
	, m_width(width)
	, m_height(height)
	, m_start(std::chrono::steady_clock::now())
{
}


void Animation::update(GraphicalEntity& g)
{
	if (m_active == false)
		return;

	float dt =
		(std::chrono::steady_clock::now() - m_start).count() / 1000000000.f;
	size_t step = dt / m_step_time_s;
	if (step >= m_steps)
	{
		if (m_repeat)
			step = 0;
		else
		{
			m_active = false;
			m_ended  = true;
			// Log::write_debug("[Animation]:\t Animation ended.");
		}
		m_start = std::chrono::steady_clock::now();
	}

	// Log::write_debug("dt : " + std::to_string(dt) + " step : " +
	// 				 std::to_string(step) + "/" + std::to_string(m_steps) +
	// 				 "(" + std::to_string(m_step_time_s) + ")");

	g.sprite.setTextureRect(sf::IntRect(x + m_width * step, y, m_width,
										m_height)); // This rectangle is safe
}

void Animation::update(sf::Vertex* v0, sf::Vector2f texCoords)
{
	assert(v0);
	assert(v0 + 1);
	assert(v0 + 2);
	assert(v0 + 3);

	if (m_active == false)
		return;

	float dt =
		(std::chrono::steady_clock::now() - m_start).count() / 1000000000.f;
	size_t step = dt / m_step_time_s;
	if (step >= m_steps)
	{
		if (m_repeat)
			step = 0;
		else
		{
			m_active = false;
			m_ended  = true;
			// Log::write_debug("[Animation]:\t Animation ended.");
		}
		m_start = std::chrono::steady_clock::now();
	}

	v0->texCoords		= texCoords + sf::Vector2f(x + m_width * step, y);
	(v0 + 1)->texCoords = texCoords + sf::Vector2f(x + m_width * (step + 1), y);
	(v0 + 2)->texCoords =
		texCoords + sf::Vector2f(x + m_width * (step + 1), y + m_height);
	(v0 + 3)->texCoords =
		texCoords + sf::Vector2f(x + m_width * step, y + m_height);
}

void Animation::set_active(bool b)
{
	m_active = b;
	m_ended  = !b;
	m_start  = std::chrono::steady_clock::now();
	// Log::write_debug("[Animation]:\t Set active to " + std::to_string(b));
}


Animation::~Animation(void)
{
}