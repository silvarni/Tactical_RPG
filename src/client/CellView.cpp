#include "client/CellView.hpp"
#include "client/settings.hpp"
#include "common/Cell.hpp"
#include "imgui.h"

#include <cassert>

CellView::CellView(const Cell* cell, sf::Vector2f position, sf::Vector2f size,
				   GraphicSystem& graphic_system)
	: mp_cell(cell)
	, m_animation(1, 1.f, size.x, size.y)
{
	// POSITION
	// 0_1
	// | |
	// 3_2
	verteces[0].position = position;
	verteces[1].position = position + sf::Vector2f(CELL_WIDTH, 0);
	verteces[2].position = position + sf::Vector2f(CELL_WIDTH, CELL_HEIGHT);
	verteces[3].position = position + sf::Vector2f(0, CELL_HEIGHT);

	// TEXTURE_KEY
	switch (mp_cell->m_type)
	{
	case EMPTY: m_texture_key = "cell_empty"; break;
	case ROCK: m_texture_key  = "cell_rock"; break;
	case WATER: m_texture_key = "cell_water"; break;
	case TREE: m_texture_key  = "cell_tree"; break;
	case HOUSE: m_texture_key = "cell_house"; break;
	default: m_texture_key	= "cell_unknown"; break;
	}

	// TEXTURE COORDINATES
	texCoords = graphic_system.get_resource_system().get_texture_coordinates(
		m_texture_key, 0);
	for (size_t i = 0ul; i < 4; i++)
	{
		verteces[i].texCoords =
			graphic_system.get_resource_system().get_texture_coordinates(
				m_texture_key, i);
	}

	//
	if (mp_cell->m_type == WATER)
		m_animation.set_nb_steps(3);
}

void CellView::reset(void)
{
	m_hovered = false;
	m_clicked = false;
}

void CellView::hovered(void)
{
	m_hovered = true;
}

void CellView::clicked(void)
{
}

void CellView::draw(void)
{
	// m_animation.update(verteces, texCoords); // TODO: Can't make it work,
	// shuuut !
	if (m_selected)
	{
		// TODO: j'en ai marre je RAGE QUIT c'est de la merde (romain: c'est moi
		// qui aie écrit ça ? lol)
	}
}

CellView::~CellView(void)
{
}
