#include "client/LobbyScene.hpp"
#include "client/Globals.hpp"
#include "client/NetworkSystem.hpp"
#include "client/settings.hpp"
#include "common/Log.hpp"
#include "common/Query.hpp"
#include "imgui.h"


LobbyScene::LobbyScene(NetworkSystem& network_system)
	: Scene("room")
	, m_network_system(network_system)
{
}

void LobbyScene::update(float dt)
{
	Query query = m_network_system.get_received();
	if (!m_network_system.is_running())
	{
		m_network_system.disconnect();
		m_active	 = false;
		m_next_scene = "splash";
		return;
	}
	sf::Int8 action;
	if (!query.endOfPacket())
	{
		query >> action;
		Log::write_debug("[GameScene]:\t Treatment of action " +
						 Query::log(action) + ".");
		switch (action)
		{
		case DISCONNECT:
			m_network_system.disconnect();
			m_active	 = false;
			m_next_scene = "splash";
			break;
		case FULL:
			m_chat.update(
				"This room is full ! Choose an other one or create !");
		case JOIN:
		{
			sf::Int8 turn_time;
			query >> Globals::get().room_name >> turn_time;
			Globals::get().turn_time = turn_time;
			m_active				 = false;
			m_next_scene			 = "room";
			break;
		}
		case ROOMS:
		{
			for (auto room : mv_rooms)
				delete room;
			mv_rooms.clear();
			std::string name;
			while (query >> name)
				mv_rooms.push_back(strdup(name.data()));
			selected_room = 0;
			if (!mv_rooms.size())
				break;
			Query out;
			out << ROOM << (sf::Int8)selected_room;
			m_network_system.send(out);
			break;
		}
		case CLIENTS:
		{
			for (auto client : mv_players)
				delete client;
			mv_players.clear();
			std::string name;
			while (query >> name)
				mv_players.push_back(strdup(name.data()));
			break;
		}
		case ROOM:
		{
			std::string name;
			sf::Int8	_nb_teams, _nb_players, _turn_time;
			query >> name >> _nb_teams >> _nb_players >> _turn_time;
			Globals::get().room_name = name;
			nb_teams				 = _nb_teams;
			nb_players				 = _nb_players;
			Globals::get().turn_time = _turn_time;
			break;
		}
		case NAME:
			query >> Globals::get().name;
			Log::write_debug("[GameScene]:\t Set player name " +
							 Globals::get().name);
			break;
		case CHAT: m_chat.update(query); break;
		default: break;
		}
	}
}

void LobbyScene::display(GraphicSystem& graphic_system,
						 AudioSystem&   audio_system)
{
	// Music
	audio_system.play_music("tavern");

	ImGui::SetNextWindowPosCenter();
	ImGui::SetNextWindowSize(sf::Vector2u(700, 550));
	if (ImGui::Begin("Lobby", nullptr, DEFAULT_WINDOW_FLAG))
	{
		ImGui::Columns(2);

		// Room list
		draw_room_list();
		ImGui::NextColumn();

		// Room creation
		draw_room_creation_menu();
		ImGui::NextColumn();

		ImGui::Separator();

		// Player list
		draw_player_list();
		ImGui::NextColumn();

		// Chat
		ImGui::Indent();
		ImGui::Spacing();
		ImGui::Text("Inn Talkings");
		Query query = m_chat.display();
		if (query.getDataSize())
			m_network_system.send(query);
		ImGui::NextColumn();

		// Back button
		ImGui::Separator();
		ImGui::Columns(1);
		ImGui::Spacing();
		if (ImGui::Button("Back", DEFAULT_BUTTON_SIZE))
			quit();

		ImGui::End();
	}


	// SFML
	graphic_system.add_entity(0, 0, "splashscreen_background", -1);
}

void LobbyScene::quit(void)
{
	Log::write_debug("[LobbyScene]\t Send request to disconnect...");
	m_network_system.send(DISCONNECT);
}

void LobbyScene::enter_tree()
{
	m_ready					 = false;
	Globals::get().turn_time = 20;
	Globals::get().gold		 = 1000;
}

void LobbyScene::exit_tree()
{
}

void LobbyScene::draw_room_list(void)
{
	ImGui::BeginGroup();
	ImGui::Indent();
	ImGui::Text("Actual rooms");

	if (ImGui::ListBox("server_rooms", &selected_room,
					   (const char**)mv_rooms.data(), mv_rooms.size(), 10))
	{
		Query query;
		query << ROOM << (sf::Int8)selected_room;
		m_network_system.send(query);
	}
	if (ImGui::Button("Join The War",
					  DEFAULT_BUTTON_SIZE - sf::Vector2f(40, 0)) &&
		selected_room >= 0)
	{
		Query query;
		query << JOIN << (sf::Int8)selected_room;
		m_network_system.send(query);
	}
	ImGui::Spacing();
	ImGui::EndGroup();
}

void LobbyScene::draw_room_creation_menu(void)
{
	ImGui::Indent();
	ImGui::BeginGroup();
	ImGui::Text("Start a war !");
	ImGui::Spacing();

	ImGui::Text("War Name");
	ImGui::SameLine();
	char room_name[255] = "";
	strcpy(room_name, Globals::get().room_name.data());
	ImGui::InputText("###room_it", room_name, 255,
					 ImGuiInputTextFlags_AutoSelectAll);
	Globals::get().room_name = std::string(room_name);
	ImGui::BeginGroup();
	ImGui::PushItemWidth(100);
	if (ImGui::InputInt("Number of Armies   ", &nb_teams))
	{
		if (nb_teams < 2)
			nb_teams = 2;
		nb_players   = nb_teams;
	}
	ImGui::InputInt("Number of Chiefs   ", &nb_players, nb_teams);
	{
		if (nb_players < nb_teams)
			nb_players = nb_teams;
	}
	ImGui::InputInt("Time duration (sec)", &Globals::get().turn_time, 5);
	ImGui::InputInt("Initial gold", &Globals::get().gold, 50);

	ImGui::PopItemWidth();

	ImGui::EndGroup();
	ImGui::Spacing();
	if (ImGui::Button("Start the war", DEFAULT_BUTTON_SIZE))
	{
		Query out;
		out << CREATE << std::string(room_name) << (sf::Int8)nb_teams
			<< (sf::Int8)nb_players << (sf::Int8)Globals::get().turn_time
			<< (sf::Int16)Globals::get().gold;
		m_network_system.send(out);
	}
	ImGui::EndGroup();
}

void LobbyScene::draw_player_list(void)
{
	ImGui::BeginGroup();
	ImGui::Spacing();
	ImGui::TextWrapped("List of war chieftains");
	ImGui::ListBox("server_players", &selected_player,
				   (const char**)mv_players.data(), mv_players.size(), 10);

	// Nickname
	char nickname[255] = "";
	strcpy(nickname, Globals::get().name.data());
	ImGui::Spacing();
	ImGui::TextWrapped("Your name:");
	ImGui::SameLine();
	if (ImGui::InputText("###player_it", nickname, 255,
						 ImGuiInputTextFlags_EnterReturnsTrue |
							 ImGuiInputTextFlags_AutoSelectAll))
	{
		if (nickname[0])
		{
			Query query;
			query << NAME << std::string(nickname);
			m_network_system.send(query);
		}
	}
	ImGui::Spacing();
	ImGui::EndGroup();
}

LobbyScene::~LobbyScene(void)
{
}
