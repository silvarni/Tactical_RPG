#include "client/EndScene.hpp"
#include "client/Globals.hpp"
#include "imgui.h"

EndScene::EndScene(NetworkSystem& network_system)
	: Scene("splash")
	, m_network_system(network_system)
{
}

void EndScene::update(float dt)
{
}

void EndScene::display(GraphicSystem& graphic_system, AudioSystem& audio_system)
{
	// AudioSystem
	audio_system.play_music("end");

	// Gui
	ImGui::SetNextWindowPosCenter();
	ImGui::SetNextWindowSize(sf::Vector2u(430, 200));
	if (ImGui::Begin("The war is over."))
	{
		ImGui::Indent();
		ImGui::Spacing();
		if (Globals::get().win)
		{
			ImGui::TextWrapped("You come back to your king, successfully and "
							   "you are generously commended to become the "
							   "King's right hand !");
		}
		else
		{
			ImGui::TextWrapped("Sadly, all your units have fallen. Your king "
							   "hates you. You lose the war.");
		}

		// style
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
							  ImVec4(255, 255, 255, 0.6));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive,
							  ImVec4(255, 255, 255, 0.8));
		// ImGui::SetWindowSize(DEFAULT_WINDOW_SIZE);

		// Images
		sf::Sprite play;
		play.setTexture(
			*graphic_system.get_resource_system().get_texture("button_play"));
		sf::Sprite exit_b;
		exit_b.setTexture(
			*graphic_system.get_resource_system().get_texture("button_exit"));

		if (ImGui::ImageButton(exit_b))
		{
			m_network_system.send(DISCONNECT);
			m_active	 = false;
			m_next_scene = "exit";
		}
		ImGui::SameLine();
		if (ImGui::ImageButton(play))
		{
			m_next_scene = "room";
			m_active	 = false;
		}
		ImGui::PopStyleColor(3);
		ImGui::End();
	}

	// SFML
	graphic_system.add_entity(0, 0, "splashscreen_background", -1);
}

void EndScene::enter_tree(void)
{
}

void EndScene::exit_tree(void)
{
}

EndScene::~EndScene(void)
{
}