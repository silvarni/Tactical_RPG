/* Copyright (C) <year>  <name of author>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of

	 General P


	ng with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/**
 * \file NetworkSystem.cpp
 * \author Romain Mekarni
 */

#include <chrono>

#include "client/NetworkSystem.hpp"
#include "common/Log.hpp"

NetworkSystem::NetworkSystem(void)
{
	m_socket.setBlocking(false);
}

void NetworkSystem::initialize(const char* ip_address, int port)
{
	m_ip_address = ip_address;
	m_port		 = port;
	m_running.test_and_set();
	m_connecting.test_and_set();
	m_receiving.test_and_set();
	m_connected.test_and_set();
	if (m_thread.joinable())
		m_thread.join();
	m_thread = std::thread(&NetworkSystem::receive, this);
}

void NetworkSystem::update(float dt)
{
	// TODO: useless ?
}

void NetworkSystem::connect(void)
{
	for (int i = 0; i < MAX_TRY && m_running.test_and_set(); i++)
	{
		Log::write_debug("[NetworkSystem]:\t Trying to join the server... (" +
						 std::to_string(i + 1) + "/" + std::to_string(MAX_TRY) +
						 ").");
		sf::Socket::Status status = m_socket.connect(m_ip_address, m_port);
		if (status == sf::Socket::Done)
		{
			m_connecting.clear();
			Log::write_debug(
				"[NetworkSystem]:\t Successfully joined the server.");
			return;
		}
		else
			std::this_thread::sleep_for(std::chrono::seconds(TIME_RETRY_S));
	}
	m_connected.clear();
	m_connecting.clear();
	m_running.clear();
	Log::write_error("[NetworkSystyem]:\t Cannot join the server.");
}

void NetworkSystem::disconnect(void)
{
	m_socket.disconnect();
	m_running.clear();
	m_thread.join();
	Log::write_debug("[NetworkSystem]:\t Closing connection with server.");
}

void NetworkSystem::receive(void)
{
	Log::write_debug(
		"[NetworkSystem::Thread_receive]:\t Lancement du thread de "
		"réception des données.");
	connect();

	while (m_running.test_and_set())
	{
		std::this_thread::sleep_for(std::chrono::duration< double >(FRAMERATE));
		if (m_received.getDataSize())
			continue;

		switch (m_socket.receive(m_received))
		{
		case sf::Socket::Done: m_receiving.clear(); break;
		case sf::Socket::Disconnected:
			Log::write_error(
				"[NetworkSystem::Thread_receive]:\t Server is unreachable.");
			m_running.clear();
			break;
		default: break;
		}
	}
	m_running.clear();
	Log::write_debug(
		"[NetworkSystem::Thread_receive]:\t Le thread termine son exécution.");
}

void NetworkSystem::send(Query query)
{
	m_socket.send(query);
}

void NetworkSystem::send(sf::Int8 in)
{
	Query query;
	query << in;
	send(query);
}

Query NetworkSystem::get_received()
{
	Query query;
	// m_receiving == false <=> packet received
	if (!m_receiving.test_and_set())
	{
		// Log::write_debug("[NetworkSystem]:\t Récupération de données " +
		// 				 std::to_string(m_received.getDataSize()) +
		// 				 "o reçues.");
		query = m_received;
		m_received.clear();
	}
	return query;
}

NetworkSystem::~NetworkSystem(void)
{
	m_running.clear();
	if (m_thread.joinable())
		m_thread.join();
}
