#include "client/Globals.hpp"

std::unique_ptr< Globals > Globals::m_self;

Globals::Globals(void)
{
}

Globals& Globals::get(void)
{
	if (!m_self)
	{
		m_self.reset(new Globals);
	}

	return *m_self;
}

Globals::~Globals(void)
{
}