#include "client/OccupierView.hpp"
#include "client/CellView.hpp"

#include "client/Globals.hpp"

OccupierView::OccupierView(Occupier* occupier)
	: mp_occupier(occupier)
	, m_graphical_entity(mp_occupier->get_position().x * CELL_WIDTH,
						 mp_occupier->get_position().y * CELL_HEIGHT,
						 mp_occupier->get_name(), 1)
{
	m_graphical_entity.sprite.setOrigin(sf::Vector2f(32, 48));
	m_graphical_entity.sprite.setScale(sf::Vector2f(0.75f, 0.75f));

	m_animation_player.update(m_graphical_entity, occupier);
}

void OccupierView::draw(GraphicSystem&   graphic_system,
						const WorldData& world_data, bool selected)
{
	// Animation
	m_animation_player.update(m_graphical_entity, mp_occupier);

	// > Dynamic decrease of range  (removed, greedy)
	// size_t max_range = mp_occupier->get_movement_points();
	// float  time_left = Globals::get().turn_time_left;
	// if (time_left / TIME_TO_TRAVEL_ONE_CELL_S < max_range)
	// 	max_range = time_left / TIME_TO_TRAVEL_ONE_CELL_S;
	// mp_occupier->update_range(world_data, max_range); // Greedy

	// > Decreasing with move points
	// if (mp_occupier->get_action() != ACTION::IDLE)
	// 	mp_occupier->update_range(world_data,
	// 							  mp_occupier->get_movement_points()); // Greedy

	// Continuous movement
	switch (mp_occupier->get_action())
	{
	default:
	{
		// if (m_animation_player.is_active())
		// 	m_animation_player.set_active(false);
		m_graphical_entity.offset_x = 0;
		m_graphical_entity.offset_y = 0;
	}
	break;

	case MOVING:
	case MOVE_AND_ATTACK:
	{
		// if (mp_occupier->path.empty())
		// {
		// 	m_animation_player.play("idle");
		// }
		// else
		// {
		std::string anim_name = "walk_" + mp_occupier->get_str_direction();
		if (!m_animation_player.is_playing(anim_name))
			m_animation_player.play(anim_name);
		m_graphical_entity.offset_x =
			(int)(CELL_WIDTH *
				  (mp_occupier->get_action_time() / TIME_TO_TRAVEL_ONE_CELL_S) *
				  ((mp_occupier->get_direction() == EAST) ||
				   (mp_occupier->get_direction() == WEST))) %
			CELL_WIDTH;
		m_graphical_entity.offset_y =
			(int)(CELL_HEIGHT *
				  (mp_occupier->get_action_time() / TIME_TO_TRAVEL_ONE_CELL_S) *
				  ((mp_occupier->get_direction() == SOUTH) ||
				   (mp_occupier->get_direction() == NORTH))) %
			CELL_HEIGHT;
		if (mp_occupier->get_direction() == WEST)
			m_graphical_entity.offset_x *= -1;
		if (mp_occupier->get_direction() == NORTH)
			m_graphical_entity.offset_y *= -1;
		// }
	}
	break;
	}

	// Color
	if (mp_occupier->get_team() != Globals::get().team)
	{
		if (mp_occupier->has_finished())
			m_graphical_entity.sprite.setColor(sf::Color(128, 150, 128));
		else
			m_graphical_entity.sprite.setColor(sf::Color(200, 150, 150));
	}
	else
	{
		if (mp_occupier->has_finished())
			m_graphical_entity.sprite.setColor(sf::Color(150, 128, 128));
		else
			m_graphical_entity.sprite.setColor(sf::Color(150, 200, 150));
	}

	if (selected)
	{
		// Change color
		if (!mp_occupier->has_finished())
			m_graphical_entity.sprite.setColor(sf::Color(200, 200, 128));

		// Add indicator
		GraphicalEntity indicator(m_graphical_entity.sprite.getPosition().x,
								  m_graphical_entity.sprite.getPosition().y,
								  "selection_indicator", 0);
		indicator.sprite.setOrigin(CELL_WIDTH / 2, CELL_HEIGHT / 2);
		indicator.sprite.setRotation(m_graphical_entity.sprite.getRotation());
		indicator.sprite.setPosition(
			mp_occupier->get_position().x * CELL_WIDTH +
				m_graphical_entity.offset_x + CELL_WIDTH / 2,
			mp_occupier->get_position().y * CELL_HEIGHT +
				m_graphical_entity.offset_y + CELL_HEIGHT / 2);
		graphic_system.add_entity(indicator);
	}


	// Movement
	m_graphical_entity.sprite.setPosition(
		mp_occupier->get_position().x * CELL_WIDTH +
			m_graphical_entity.offset_x + CELL_WIDTH / 2,
		mp_occupier->get_position().y * CELL_HEIGHT +
			m_graphical_entity.offset_y + CELL_HEIGHT / 2);

	// Add occupier to graphic system
	graphic_system.add_entity(m_graphical_entity);
}

OccupierView::~OccupierView(void)
{
}