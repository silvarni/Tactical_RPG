#include "client/WorldView.hpp"
#include "client/Globals.hpp"
#include "client/Input.hpp"
#include "common/Log.hpp"
#include "common/Occupier.hpp"
#include "imgui.h"

#include <cassert>

WorldView::WorldView(WorldData& world_data, GraphicSystem& graphic_system,
					 NetworkSystem& network_system, AudioSystem& audio_system)
	: m_world_data(world_data)
	, m_network_system(network_system)
	// Initialize with correct capacity (so adding is real O(n))
	, m_cells(WORLD_HEIGHT,
			  std::vector< std::shared_ptr< CellView > >(WORLD_WIDTH))
	, m_focus(-1, -1)
	, m_view_manager(VIEW_SIZE, VIEW_SIZE, graphic_system)
	, m_map(sf::Quads)
	, m_graphical_hover(0, 0, "hover", 1)
	, m_graphical_clicked(0, 0, "clicked", 1)
	, m_graphic_system(graphic_system)
	, m_audio_system(audio_system)
{
}

void WorldView::reinitialize(void)
{
	m_map.clear();
	m_occupiers.clear();
	m_cells.clear();
	for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
	{
		m_cells.emplace_back();
		for (size_t x = 0ul; x < WORLD_WIDTH; ++x)
		{
			// Create CellView
			m_cells[y].emplace_back(new CellView(
				&m_world_data.get_cell(x, y),
				sf::Vector2f(x * CELL_WIDTH, y * CELL_HEIGHT),
				sf::Vector2f(CELL_WIDTH, CELL_HEIGHT), m_graphic_system));

			// Add the cell verteces to graphic system
			for (size_t i = 0ul; i < 4; ++i)
			{
				m_graphic_system.add_to_vertex_array(
					m_cells[y][x]->verteces[i]);
			}

			// Create OccupierView
			if (m_world_data.get_cell(x, y).m_occupier)
			{
				m_occupiers.emplace_back(
					m_world_data.get_cell(x, y).m_occupier);
			}
		}
		m_width  = WORLD_WIDTH * CELL_WIDTH;
		m_height = WORLD_HEIGHT * CELL_HEIGHT;
		m_view_manager.set_world_size(sf::Vector2f(m_width, m_height));
	}
}

void WorldView::reinitialize_occupiers(void)
{
	Log::write_debug("[WorldView]:\t Reinitialized occupiers.");
	m_occupiers.clear();
	for (size_t x = 0ul; x < WORLD_WIDTH; ++x)
	{
		for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
		{
			// Create OccupierView
			if (m_world_data.get_cell(x, y).m_occupier)
			{
				m_occupiers.emplace_back(
					m_world_data.get_cell(x, y).m_occupier);
			}
		}
	}
	next_unit();
}

CellView& WorldView::get_cell(size_t x, size_t y)
{
	assert(x <= WORLD_WIDTH && y <= WORLD_HEIGHT);
	return *m_cells[y][x];
}

void WorldView::update(float dt)
{
	m_dt = dt;

	if (m_world_data.get_occupiers().size() != m_occupiers.size())
	{
		reinitialize_occupiers();
		next_unit();
	}


	if (m_world_data.actions.empty())
	{
		Globals::get().can_check_finished = true;
		if (Globals::get().turn)
		{
			if (m_actor && m_actor->get_action() != ACTION::IDLE &&
				!Input::is_any_key_pressed())
			{
				m_focus.x = m_actor->get_position().x * CELL_WIDTH;
				m_focus.y = m_actor->get_position().y * CELL_HEIGHT;
			}
		}
		m_view_manager.update(dt, m_focus);
	}
	else
	{
		Globals::get().can_check_finished = false;
		// Log::write_debug("[WorldView]:\tThere are actions to show (" +
		// 				 std::to_string(m_world_data.actions.size()) + ").");
		// Something to show
		Action& top = m_world_data.actions.front();


		bool must_pop = false;


		OccupierView* target_view = find_occupier_view(top.actor);

		if (target_view == nullptr)
		{
			Log::write_debug("[WorldView]:\tCannot find matching occupier at " +
							 Log::to_string(top.position) + ".");
			must_pop = true;
		}
		else
		{
			// Camera focus it
			m_focus.x =
				target_view->get_graphical_entity().sprite.getPosition().x;
			m_focus.y =
				target_view->get_graphical_entity().sprite.getPosition().y;
			if (!Globals::get().positioning)
				m_view_manager.set_auto_focus(true);

			switch (top.action)
			{
			default:
				Log::write_debug("[WorldView]:\t Unhandled action : " +
								 std::to_string(top.action) + ".");
				must_pop = true;
				break;

			case ACTION::MOVING:
			{
				// Managed in OccupierView
				if (top.actor->path.empty())
					must_pop = true;
				// Follow actor during movement
				m_focus.x =
					target_view->get_graphical_entity().sprite.getPosition().x;
				m_focus.y =
					target_view->get_graphical_entity().sprite.getPosition().y;

				// Everything's ok
				// else
				// 	Log::write_debug(
				// 		"[WorldView]:\t Can't pop due to non-empty path
				// "
				// 		"(number of cell to travel : " +
				// 		std::to_string(top.actor.path.size()) + ").");
			}
			break;

			case ACTION::MOVE_AND_ATTACK:
			{
				if (top.actor->path.empty())
				{
					top.action = ACTION::ATTACKING;
					top.actor->set_action(ACTION::ATTACKING);
					// Log::write_debug("[WorldView]:\tEnd "
					// 				 "Move-and-attack, now attack.");
				}
				else
				{
					// Log::write_debug("[WorldView]:\tStuck in
					// move-and-attack.");
					// Follow actor during movement
					m_focus.x = target_view->get_graphical_entity()
									.sprite.getPosition()
									.x;
					m_focus.y = target_view->get_graphical_entity()
									.sprite.getPosition()
									.y;
				}
			}
			break;

			case ACTION::ATTACKING:
			{
				// Log::write_debug("[WORLD_VIEW]:\tWaiting for animation "
				// 				 "of attacking in " +
				// 				 Log::to_string(top.position));

				// Log::write_debug("[WorldView]:\t Actor path is
				// empty.");
				std::string facing;

				auto pos   = target_view->get_occupier()->get_position();
				auto e_pos = target_view->get_occupier()->get_target();
				if (e_pos.x > pos.x)
					facing = "right";
				else if (e_pos.x < pos.x)
					facing = "left";
				else if (e_pos.y > pos.y)
					facing = "down";
				else if (e_pos.y < pos.y)
					facing = "top";
				std::string anim_name =
					target_view->get_occupier()->get_weapon_name() +
					"_attack_" + facing;

				if (!m_is_anim)
				{
					m_is_anim = true;
					target_view->get_animation_player().play(anim_name);
					m_audio_system.play_sound(
						target_view->get_occupier()->get_weapon_name() +
						"_attack");
				}
				else if (m_is_anim &&
						 target_view->get_animation_player().is_ended(
							 anim_name))
				{
					m_is_anim = false;
					must_pop  = true;
					target_view->get_occupier()->set_action(ACTION::IDLE);
				}
			}
			break;

			case ACTION::ATTACKED:
			{
				if (!m_is_anim)
				{
					m_is_anim = true;
					target_view->get_animation_player().play("hurt");
					m_audio_system.play_sound("hurt");
				}
				else if (m_is_anim &&
						 target_view->get_animation_player().is_ended("hurt"))
				{
					m_is_anim = false;
					must_pop  = true;
				}
			}
			break;

			case ACTION::DIE:
			{
				if (!m_is_anim)
				{
					m_is_anim = true;
					target_view->get_animation_player().play("die");
					m_audio_system.play_sound("die");
				}
				else if (m_is_anim &&
						 target_view->get_animation_player().is_ended("die"))
				{
					m_is_anim = false;
					must_pop  = true;
				}
			}
			break;
			}

			m_view_manager.update(dt, m_focus);
			if (must_pop)
			{
				m_world_data.actions.pop_front();
				// Log::write_debug("[WorldView]:\t Pop action !");
			}
		}
	}
	update_controls();
}

void WorldView::display(void)
{
	// GUI
	print_gui();

	m_view_manager.display();

	// Positioning mode shows spawns
	if (Globals::get().positioning)
	{
		m_graphic_system.set_resize();
		for (auto& spawn : m_spawns)
			add_cell_color(spawn.x, spawn.y, sf::Color(200, 200, 200));
	}

	// Highlight Hovered cell
	sf::Vector2f global_mouse_pos =
		m_graphic_system.get_global_mouse_position();
	size_t x = global_mouse_pos.x / CELL_WIDTH;
	size_t y = global_mouse_pos.y / CELL_HEIGHT;

	if (x < WORLD_WIDTH && y < WORLD_HEIGHT && x >= 0 && y >= 0)
	{
		if (m_hovered)
		{
			m_hovered->reset();
		}
		if (m_last_cell_pos != sf::Vector2u(x, y))
			m_hovered_time = std::chrono::steady_clock::now();

		m_last_cell_pos = sf::Vector2u(x, y);
		m_hovered		= m_cells[y][x].get();
		m_graphical_hover.sprite.setPosition(x * CELL_WIDTH, y * CELL_HEIGHT);

		if (m_hovered)
			m_hovered->hovered();
	}

	// Draw occupiers
	// draw_cells();
	draw_occupiers();

	// Add everything
	m_graphic_system.add_entity(m_graphical_hover);
	m_graphic_system.add_entity(m_graphical_clicked);
}

void WorldView::print_gui(void)
{
	// Positionning
	if (Globals::get().positioning)
	{
		ImGui::SetNextWindowSize(sf::Vector2u(250, 370));
		if (ImGui::Begin("Tactic mode"))
		{
			// Small Description
			ImGui::TextWrapped(
				"Please, select one of following mercenaries and place it in "
				"the grey area. Beware to your gold (displayed in top menu). ");
			ImGui::Spacing();
			ImGui::Separator();
			ImGui::Spacing();

			// Buttons
			for (size_t i = OccupierFactory::TYPE::NONE + 1;
				 i < OccupierFactory::TYPE::MAX; ++i)
			{
				ImGui::BeginGroup();
				// Button
				std::string unit_name =
					OccupierFactory::get_unit_name((OccupierFactory::TYPE)i);
				ImGui::Text(unit_name.c_str());
				sf::Sprite im;
				im.setTexture(
					*m_graphic_system.get_resource_system().get_texture(
						unit_name + "_preview"));
				im.setScale(sf::Vector2f(0.6, 0.6));
				if (ImGui::ImageButton(im))
				{
					m_positioning_type = (OccupierFactory::TYPE)i;
				}

				// Name

				Occupier* ex_to_del = OccupierFactory::create_unit(
					sf::Vector2u(0, 0), 0, (OccupierFactory::TYPE)i);

				// Description

				sf::Sprite damage;
				damage.setTexture(
					*m_graphic_system.get_resource_system().get_texture(
						"damage_icon"));
				damage.setScale(sf::Vector2f(0.3, 0.3));
				sf::Sprite health;
				health.setTexture(
					*m_graphic_system.get_resource_system().get_texture(
						"health_icon"));
				health.setScale(sf::Vector2f(0.3, 0.3));
				sf::Sprite speed;
				speed.setTexture(
					*m_graphic_system.get_resource_system().get_texture(
						"speed_icon"));
				speed.setScale(sf::Vector2f(0.3, 0.3));
				sf::Sprite money;
				money.setTexture(
					*m_graphic_system.get_resource_system().get_texture(
						"money_icon"));
				money.setScale(sf::Vector2f(0.3, 0.3));

				if ((int)i != (int)m_positioning_type)
					im.setColor(sf::Color(45, 45, 45));

				ImGui::SameLine();
				ImGui::BeginGroup();
				ImGui::Columns(2);
				ImGui::Image(damage);
				ImGui::SameLine();
				ImGui::Text(
					std::to_string((int)ex_to_del->get_damage()).c_str());
				ImGui::NextColumn();
				ImGui::Image(health);
				ImGui::SameLine();
				ImGui::Text(
					std::to_string((int)ex_to_del->get_health()).c_str());
				ImGui::NextColumn();
				ImGui::Image(speed);
				ImGui::SameLine();
				ImGui::Text(
					std::to_string((int)ex_to_del->get_speed()).c_str());
				ImGui::NextColumn();
				ImGui::Image(money);
				ImGui::SameLine();
				ImGui::Text(std::to_string((int)OccupierFactory::get_cost(
											   (OccupierFactory::TYPE)i))
								.c_str());
				ImGui::Columns(1);
				ImGui::EndGroup();

				delete (ex_to_del);
				ImGui::EndGroup();
			}

			ImGui::End();
		}
	}

	// GUI
	// Sprite of Zoom +
	sf::Sprite zp(
		*m_graphic_system.get_resource_system().get_texture("button_zoom_+"));
	zp.scale(0.7, 0.7);
	// Sprite of zoom -
	sf::Sprite zm(
		*m_graphic_system.get_resource_system().get_texture("button_zoom_-"));
	zm.scale(0.7, 0.7);

	sf::Sprite focus(
		*m_graphic_system.get_resource_system().get_texture("focus"));
	focus.scale(0.7, 0.7);

	ImGui::SetNextWindowPos(
		sf::Vector2f(Globals::get().current_window_size.x - 100,
					 50)); // Position of Zoom + / - buttons

	// Zoom buttons
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
	if (ImGui::Begin("", NULL,
					 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize))
	{
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0, 0, 0, 0));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered,
							  ImVec4(255, 255, 255, 0.6));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive,
							  ImVec4(255, 255, 255, 0.8));

		if (ImGui::ImageButton(zp))
		{

			m_view_manager.zoom(0.8);
		}
		if (ImGui::ImageButton(zm))
		{

			m_view_manager.zoom(1.25);
		}
		if (ImGui::ImageButton(focus))
		{
			m_view_manager.set_auto_focus(true);
		}

		ImGui::PopStyleColor(3);
		ImGui::End();
	}
	ImGui::PopStyleColor();

	// Depending on the mode
	switch (m_mode)
	{
	default: break;
	case MODE::IDLE: {
	}
	break;

	case MODE::SELECTED: {
	}
	break;

	case MODE::TARGETING:
	{

		size_t x = m_target->get_position().x;
		size_t y = m_target->get_position().y;
		Query  query;
		query << WORLD << ATTACK << m_actor->get_position()
			  << sf::Vector2u(x, y);
		m_network_system.send(query);
		Log::write_debug("[WorldView]:\t "
						 "Asks to	"
						 "attack from " +
						 Log::to_string(m_actor->get_position()) + " to " +
						 Log::to_string(sf::Vector2u(x, y)) + ".");
		ImGui::CloseCurrentPopup();
		m_mode   = MODE::SELECTED;
		m_target = nullptr;
	}
	break;
	}

	// Print info under mouse
	print_occupier_information();
}

void WorldView::draw_occupiers(void)
{
	if (m_actor == 0 && m_occupiers.size())
		next_unit();

	// Draw range
	if (m_actor && !Globals::get().positioning)
	{
		m_graphic_system.set_resize();
		for (auto& p_cell : m_actor->attack_range)
			add_cell_color(p_cell->x, p_cell->y, sf::Color(200, 200, 255));
		for (auto& p_cell : m_actor->move_range)
			add_cell_color(p_cell->x, p_cell->y, sf::Color(200, 255, 200));
	}

	for (auto& occupier : m_occupiers)
	{
		// TODO: in view ?
		sf::Vector2u o_pos = occupier.get_occupier()->get_position();
		if (m_actor &&
			occupier.get_occupier()->get_team() != Globals::get().team &&
			WorldData::distance(m_actor->get_position(), o_pos) <
				m_actor->get_range())
		{
			// add_line(m_actor->get_position(), o_pos, sf::Color::Red);
			add_cell_color(o_pos.x, o_pos.y, sf::Color(255, 150, 150), true);
		}
		occupier.draw(m_graphic_system, m_world_data,
					  occupier.get_occupier() == m_actor);

		// TEMP : allow to focus UN-IDLE units
		// if (occupier.get_occupier()->get_action() != ACTION::IDLE ||
		// 	m_focus == sf::Vector2f(-1, -1))
		// {
		// 	sf::Vector2u focus =
		// occupier.get_occupier()->get_position();
		// 	m_focus = sf::Vector2f(focus.x * CELL_WIDTH, focus.y *
		// CELL_HEIGHT);
		// }
	}
}

void WorldView::add_line(sf::Vector2u a, sf::Vector2u b, sf::Color color)
{
	sf::Vertex line[2];
	line[0].position = sf::Vector2f(a.x * CELL_WIDTH, a.y * CELL_HEIGHT);
	line[0].color	= color;
	line[1].position = sf::Vector2f(b.x * CELL_WIDTH, b.y * CELL_HEIGHT);
	line[1].color	= color;
	m_graphic_system.add_to_vertex_array(line[0]);
	m_graphic_system.add_to_vertex_array(line[1]);
}

void WorldView::add_cell_color(int x, int y, sf::Color color, bool degrade)
{
	for (size_t i = 0ul; i < 4; ++i)
	{
		if (degrade && i < 2)
			m_cells[y][x]->verteces[i].color = sf::Color::White;
		else
			m_cells[y][x]->verteces[i].color = color;
		m_graphic_system.add_to_vertex_array(m_cells[y][x]->verteces[i]);
	}
}

void WorldView::draw_cells(void)
// O(width * height)
{
	for (size_t i = 0ul; i < WORLD_HEIGHT; ++i)
		for (size_t j = 0ul; j < WORLD_HEIGHT; ++j)
			m_cells[i][j]->draw();
}

std::string WorldView::get_visual_name(size_t x, size_t y) const
{
	return m_cells[y][x]->get_texture_key();
}

void WorldView::update_controls(void)
{
	sf::Vector2f global_mouse_pos =
		m_graphic_system.get_global_mouse_position();
	int x = std::max(0, (int)(global_mouse_pos.x / CELL_WIDTH));
	int y = std::max(0, (int)(global_mouse_pos.y / CELL_HEIGHT));


	// Tab = next unit
	Control& next = Input::is_action_pressed("next_unit");
	if (next)
	{
		next.pressed = false;
		next_unit();
	}

	// Left click management
	Control& left_click = Input::is_action_pressed("mouse_left_released");
	if (left_click)
	{
		left_click.pressed			   = false;
		Occupier* target_cell_occupier = m_world_data.get_cell(x, y).m_occupier;

		// Left click on positioning and unit selected
		bool in_spawns = false;
		for (auto& spawn_pos : m_spawns)
		{
			if ((int)spawn_pos.x == x && (int)spawn_pos.y == y)
			{
				in_spawns = true;
				break;
			}
		}

		if (Globals::get().positioning &&
			m_positioning_type != OccupierFactory::TYPE::NONE && in_spawns)
		{
			Query new_occupier;
			new_occupier << CREATE_OCCUPIER << x << y << Globals::get().team
						 << m_positioning_type;
			m_network_system.send(new_occupier);
			Log::write_debug(
				"[WorldView]:\t Asked to create occupier of type " +
				std::to_string(m_positioning_type) + ".");
			m_positioning_type = OccupierFactory::TYPE::NONE;
		}
		else
		{
			switch (m_mode)
			{
			default: break;
			case IDLE:
			{
				// Highlight Clicked cell
				// Reset last clicked if necessary
				if (m_clicked)
					m_clicked->reset();
				m_clicked = m_hovered;
				m_clicked->clicked();
				m_graphical_clicked.sprite.setPosition(x * CELL_WIDTH,
													   y * CELL_HEIGHT);

				// Select your unit with left click
				if (target_cell_occupier &&
					target_cell_occupier->get_team() == Globals::get().team)
				{
					select(target_cell_occupier);
				}
			}
			break;

			case SELECTED:
			{
				// If an actor is selected, maybe he wants to move
				sf::Vector2u pos = m_actor->get_position();
				if (m_actor && m_actor->get_action() == ACTION::IDLE)
				{
					if (pos !=
						sf::Vector2u(x, y)) // if not re-clicking the actor
					{
						Occupier* target_cell_occupier =
							m_world_data.get_cell(x, y).m_occupier;

						if (target_cell_occupier)
						{
							// Selected another unit
							if (target_cell_occupier->get_team() ==
								Globals::get().team)
							{
								select(target_cell_occupier);
							}

							// Selected enemy unit, see if in range
							else if (WorldData::distance(
										 m_actor->get_position(),
										 target_cell_occupier
											 ->get_position()) <=
									 m_actor->get_range())
							{
								m_mode   = MODE::TARGETING;
								m_target = target_cell_occupier;
							}
							else
							{
								for (auto& cell : m_actor->attack_range)
								{
									if (target_cell_occupier ==
										cell->m_occupier)
									{
										m_mode   = MODE::TARGETING;
										m_target = target_cell_occupier;
										break;
									}
								}
							}
						}

						// Actor clicking on empty cell
						else if (!ImGui::IsAnyItemHovered())
						{
							Query query;
							query << WORLD << MOVE << pos << sf::Vector2u(x, y);
							m_network_system.send(query);
							Log::write_debug(
								"[WorldView]:\t Asks server for moving " +
								Log::to_string(pos) + " to " +
								Log::to_string(sf::Vector2u(x, y)));
							// m_actor = nullptr;
							// m_mode = IDLE;
						}
					}
					// Unselecting actor
					else
					{
						m_actor = nullptr;
						m_mode  = IDLE;
					}
				}
				// Else we clicked an empty cell with no selection
				else
				{
					Occupier* target_cell_occupier =
						m_world_data.get_cell(x, y).m_occupier;
					if (target_cell_occupier &&
						target_cell_occupier->get_team() == Globals::get().team)
					{
						// We are clicking on another unit while the first is
						// busy,
						// Meaning we want to go faster than view,
						// Disable it then.
						select(target_cell_occupier);
						m_view_manager.set_auto_focus(false);
					}
					else
						m_mode = IDLE;
				}
			}
			break;

			case MODE::TARGETING:
			{
				if (target_cell_occupier == nullptr &&
					!ImGui::IsAnyItemHovered())
					m_mode = SELECTED;
			}
			break;
			}
		}
	}
}

void WorldView::print_occupier_information(void)
{
	sf::Vector2f mouse_position = m_graphic_system.get_global_mouse_position();
	size_t		 x				= mouse_position.x / CELL_WIDTH;
	size_t		 y				= mouse_position.y / CELL_HEIGHT;
	Occupier*	occupier		= m_world_data.get_cell(x, y).m_occupier;

	// Quick exit
	if (occupier == nullptr)
		return;

	sf::Vector2f window_position(m_graphic_system.get_mouse_position() +
								 sf::Vector2i(30, 10));
	std::string window_title = "Ally Preview";

	if (occupier->get_team() != Globals::get().team)
		window_title = "Enemy Preview";

	ImGui::SetNextWindowPos(window_position);
	if (ImGui::Begin(window_title.c_str(), nullptr,
					 DEFAULT_WINDOW_FLAG | ImGuiWindowFlags_NoTitleBar))
	{
		sf::Sprite preview;
		preview.setTexture(*m_graphic_system.get_resource_system().get_texture(
			occupier->get_name() + "_preview"));
		ImGui::Indent();

		// Image
		ImGui::Image(preview);

		// Name
		ImGui::Text(occupier->get_name().c_str());

		// Distance
		if (m_actor)
			ImGui::Text(
				("Flying Distance: " +
				 std::to_string(WorldData::distance(occupier->get_position(),
													m_actor->get_position())))
					.c_str());
		else
		{
			ImGui::SameLine();
			ImGui::Text(("(" + std::to_string(occupier->get_position().x) +
						 "," + std::to_string(occupier->get_position().y) + ")")
							.c_str());
		}

		ImGui::Text(
			("Life: " + std::to_string((int)occupier->get_health())).c_str());
		if (m_actor)
		{
			ImGui::SameLine();
			ImGui::Text(("(" + std::to_string((int)(occupier->get_health() /
														m_actor->get_damage() +
													0.5)) +
						 " hits)")
							.c_str());
		}

		ImGui::Text(("Move: " + std::to_string(occupier->get_movement_points()))
						.c_str());
		ImGui::SameLine();
		ImGui::Text((" / " + std::to_string(occupier->get_speed())).c_str());


		ImGui::End();
	}
}

void WorldView::clear(void)
{
	m_occupiers.clear();
	m_cells.clear();
	m_clicked = nullptr;
	m_hovered = nullptr;
	m_actor   = nullptr;
	m_target  = nullptr;
	m_mode	= IDLE;
}

OccupierView* WorldView::find_occupier_view(const Occupier* occupier)
{
	// Find matching OccupierView (Dirty O(n))
	for (auto& occupier_view : m_occupiers)
	{
		if (occupier_view.get_occupier() == occupier)
		{
			// target_view = &occupier_view;
			// Log::write_debug("[WORLD_VIEW]:"
			// 				 "\tFound matching "
			// 				 "occupier.");
			return &occupier_view;
		}
	}
	return nullptr;
}

void WorldView::new_turn(void)
{
	m_view_manager.new_turn();

	m_actor = nullptr;

	m_world_data.actions.clear();
}

void WorldView::next_unit(void)
{
	bool next = false;
	for (OccupierView& ov : m_occupiers)
	{
		if (next)
		{
			Occupier* o = ov.get_occupier();
			if (o->get_team() == Globals::get().team && !o->has_finished() &&
				o->is_alive())
			{
				select(o);
				return;
			}
		}
		if (ov.get_occupier() == m_actor)
			next = true;
	}

	// We didn't find one of our team
	for (OccupierView& ov : m_occupiers)
	{
		Occupier* o = ov.get_occupier();
		if (o != m_actor && o->get_team() == Globals::get().team &&
			!o->has_finished() && o->is_alive())
		{
			select(o);
			return;
		}
	}

	// Nothing satifying, nevermind...
}

void WorldView::print_current_actor_information(void)
{
	if (m_actor)
	{
		ImGui::BeginGroup();
		sf::Sprite damage;
		damage.setTexture(
			*m_graphic_system.get_resource_system().get_texture("damage_icon"));
		damage.setScale(sf::Vector2f(0.3, 0.3));
		sf::Sprite health;
		health.setTexture(
			*m_graphic_system.get_resource_system().get_texture("health_icon"));
		health.setScale(sf::Vector2f(0.3, 0.3));
		sf::Sprite speed;
		speed.setTexture(
			*m_graphic_system.get_resource_system().get_texture("speed_icon"));
		speed.setScale(sf::Vector2f(0.3, 0.3));
		sf::Sprite money;
		money.setTexture(
			*m_graphic_system.get_resource_system().get_texture("money_icon"));
		money.setScale(sf::Vector2f(0.3, 0.3));

		// Little preview
		ImGui::Image(*m_graphic_system.get_resource_system().get_texture(
						 m_actor->get_name() + "_preview"),
					 sf::Color::White, sf::Color::White);

		ImGui::SameLine();
		// Next
		sf::Sprite arrow(
			*m_graphic_system.get_resource_system().get_texture("arrow_right"));
		if (ImGui::ImageButton(arrow))
		{
			next_unit();
		}

		// Name
		ImGui::SameLine();
		ImGui::TextWrapped(m_actor->get_name().c_str());


		// Life
		ImGui::Image(health);
		ImGui::SameLine();
		ImGui::ProgressBar(m_actor->get_health() / m_actor->get_max_health(),
						   sf::Vector2f(100, 15));

		// Damage
		ImGui::Image(damage);
		ImGui::SameLine();
		ImGui::Text((std::to_string(m_actor->get_damage())).c_str());

		// Move
		ImGui::Image(speed);
		ImGui::SameLine();
		ImGui::Text((std::to_string(m_actor->get_movement_points()) + " / " +
					 std::to_string(m_actor->get_speed()))
						.c_str());


		ImGui::EndGroup();
	}
}

void WorldView::select(Occupier* actor)
{
	if (actor->is_alive())
	{
		m_actor = actor;
		m_actor->update_range(m_world_data);
		m_mode	= SELECTED;
		m_focus.x = actor->get_position().x * CELL_WIDTH;
		m_focus.y = actor->get_position().y * CELL_HEIGHT;
	}
}

WorldView::~WorldView(void)
{
}
