#include "ViewManager.hpp"
#include "client/Input.hpp"
#include "common/WorldData.hpp"
ViewManager::ViewManager(size_t width, size_t height,
						 GraphicSystem& graphic_system)
	: m_width(width)
	, m_height(height)
	, m_view(sf::Vector2f(width, height), sf::Vector2f(width, height))
	, m_graphic_system(graphic_system)
{
}

void ViewManager::update(float dt, sf::Vector2f focus)
{
	// if resized
	if (m_width != Globals::get().current_window_size.x ||
		m_height != Globals::get().current_window_size.y)
	{
		m_width  = Globals::get().current_window_size.x;
		m_height = Globals::get().current_window_size.y;
		m_view.setSize(sf::Vector2f(m_width, m_height));
	}
	m_dt		   = dt;
	bool has_moved = update_auto(dt); // returns true if has voluntary moved
	if (m_auto_focus && has_moved)
		m_auto_focus = false; // set true in new turn

	if ((Input::is_action_pressed("focus") || m_auto_focus) &&
		focus != sf::Vector2f(-1, -1) && !Globals::get().positioning)
	{
		m_view.move((focus - m_view.getCenter()) * (dt * 2));
	}
}

bool ViewManager::update_auto(float dt)
{
	bool has_moved = false;
	m_dt		   = dt;
	// Scroll by mouse
	if (Globals::get().mouse_scroll_enabled)
	{
		sf::Vector2i mouse_pos = m_graphic_system.get_mouse_position();
		if (mouse_pos.x < (int)MOUSE_MARGIN &&
			m_view.getCenter().x > m_view.getSize().x / 2)
		{
			m_view.move(-m_speed_view * dt, 0);
			has_moved = true;
		}
		else if (mouse_pos.x > (int)(Globals::get().current_window_size.x -
									 MOUSE_MARGIN) &&
				 m_view.getCenter().x < m_width - m_view.getSize().x / 2)
		{
			m_view.move(m_speed_view * dt, 0);
			has_moved = true;
		}
		if (mouse_pos.y < (int)MOUSE_MARGIN &&
			m_view.getCenter().y > m_view.getSize().x / 2)
		{
			m_view.move(0, -m_speed_view * dt);
			has_moved = true;
		}
		else if (mouse_pos.y > (int)(Globals::get().current_window_size.y -
									 MOUSE_MARGIN) &&
				 m_view.getCenter().y < m_height - m_view.getSize().x / 2 +
											CELL_HEIGHT /*Menu bar*/)
		{
			m_view.move(0, m_speed_view * dt);
			has_moved = true;
		}
	}

	// Scroll with arrows
	if (Input::is_action_pressed("ui_left") &&
		m_view.getCenter().x > VIEW_SIZE / 2)
	{
		m_view.move(-m_speed_view * dt, 0);
		has_moved = true;
	}
	else if (Input::is_action_pressed("ui_right") &&
			 m_view.getCenter().x < m_world_width - VIEW_SIZE / 2)
	{
		m_view.move(m_speed_view * dt, 0);
		has_moved = true;
	}
	if (Input::is_action_pressed("ui_up") &&
		m_view.getCenter().y > VIEW_SIZE / 2)
	{
		m_view.move(0, -m_speed_view * dt);
		has_moved = true;
	}
	else if (Input::is_action_pressed("ui_down") &&
			 m_view.getCenter().y < m_world_height - VIEW_SIZE / 2)
	{
		m_view.move(0, m_speed_view * dt);
		has_moved = true;
	}

	check_bounds();
	return has_moved;
}

void ViewManager::display(void)
{
	double zoom_speed = 4;

	// Zoom with +
	if (Input::is_action_pressed("ui_plus"))
	{
		if (m_view.getSize().x > MIN_VIEW_SIZE)
		{
			if (m_view.getCenter().x > VIEW_SIZE / 2 &&
				m_view.getCenter().x < m_world_width - VIEW_SIZE / 2 &&
				m_view.getCenter().y > VIEW_SIZE / 2 &&
				m_view.getCenter().y < m_world_height - VIEW_SIZE / 2)
			{
				m_view.move((m_graphic_system.get_global_mouse_position() -
							 m_view.getCenter()) *
							(float)m_dt * 2.5f);
			}
			m_view.zoom(1 -
						zoom_speed *
							(Input::is_action_pressed("ui_plus") ? m_dt : 1.f));
		}
	}

	// Zoom with -
	if (Input::is_action_pressed("ui_minus"))
	{
		if (m_view.getSize().x < m_world_width)
		{
			m_view.zoom(1 +
						zoom_speed * (Input::is_action_pressed("ui_minus")
										  ? m_dt
										  : 1.f));
		}
	}

	// Wheel zoom
	Control& wheel = Input::is_action_pressed("ui_wheel");
	if (wheel.pressed)
	{
		if ((wheel.is_toggle_control && m_view.getSize().x > MIN_VIEW_SIZE) ||
			(!wheel.is_toggle_control &&
			 m_view.getSize().y < m_world_height - 100))
		{
			// m_view.move((m_graphic_system.get_global_mouse_position() -
			// 			 m_view.getCenter()) *
			// 			(float)m_dt * 2.5f * (float)(std::abs(d)));
			m_view.zoom(1 + 6 * ((wheel.is_toggle_control) ? -m_dt : m_dt));
			// m_view.setCenter(m_graphic_system.get_global_mouse_position());
		}
		wheel.pressed			= false;
		wheel.is_toggle_control = false;
	}

	// Drag the map
	if (Input::is_action_pressed("mouse_right_dragging"))
	{
		m_auto_focus = false;
		auto delta =
			m_graphic_system.get_mouse_position() - m_last_mouse_position;
		sf::FloatRect scroll_area(VIEW_SIZE / 2, VIEW_SIZE / 2,
								  m_world_width * CELL_WIDTH - VIEW_SIZE / 2,
								  m_world_height * CELL_HEIGHT - VIEW_SIZE / 2);
		if (scroll_area.contains(
				(m_view.getCenter() + sf::Vector2f(-delta.x, -delta.y))))
			m_view.move(-delta.x, -delta.y);
	}
	m_last_mouse_position = m_graphic_system.get_mouse_position();

	check_bounds();
	m_graphic_system.set_view(m_view);
}

void ViewManager::check_bounds(void)
{
	sf::Vector2f view_center = m_view.getCenter(), view_size = m_view.getSize();
	if (view_center.x - view_size.x / 2 < 0)
		m_view.move(view_size.x / 2 - view_center.x, 0);
	if (view_center.y - view_size.y / 2 < 0)
		m_view.move(0, view_size.y / 2 - view_center.y);
	if (view_center.y + view_size.y / 2 > m_world_height)
		m_view.move(0, (int)m_world_height - view_center.y - view_size.y / 2);
	if (view_center.x + view_size.x / 2 > m_world_width)
		m_view.move((int)m_world_width - view_center.x - view_size.x / 2, 0);
}

void ViewManager::zoom(float delta)
{
	if (delta > 1 && m_view.getSize().x < WORLD_WIDTH * CELL_WIDTH)
	{
		m_view.zoom(delta);
	}
	if (delta < 1 && m_view.getSize().x > MIN_VIEW_SIZE)
	{
		m_view.zoom(delta);
	}
}

void ViewManager::set_world_size(sf::Vector2f size)
{
	m_world_width  = size.x;
	m_world_height = size.y;
}

void ViewManager::new_turn(void)
{
	if (Globals::get().auto_focus_enabled && !Globals::get().positioning)
		m_auto_focus = true;
}

ViewManager::~ViewManager()
{
}