#include "client/AnimationPlayer.hpp"
#include "common/Log.hpp"

AnimationPlayer::AnimationPlayer(void)
	: m_animations()
{
	// Spear attack
	add_animation("spear_attack_up", 0, 4 * 64, 64, 64, 8, 0.1);
	add_animation("spear_attack_left", 0, 5 * 64, 64, 64, 8, 0.1);
	add_animation("spear_attack_down", 0, 6 * 64, 64, 64, 8, 0.1);
	add_animation("spear_attack_right", 0, 7 * 64, 64, 64, 8, 0.1);

	// Walk
	add_animation("walk_up", 0, 8 * 64, 64, 64, 6, 0.1);
	add_animation("walk_left", 0, 9 * 64, 64, 64, 6, 0.1);
	add_animation("walk_down", 0, 10 * 64, 64, 64, 6, 0.1);
	add_animation("walk_right", 0, 11 * 64, 64, 64, 6, 0.1);

	// Knife attack
	add_animation("knife_attack_up", 0, 12 * 64, 64, 64, 5, 0.1);
	add_animation("knife_attack_left", 0, 13 * 64, 64, 64, 5, 0.1);
	add_animation("knife_attack_down", 0, 14 * 64, 64, 64, 5, 0.1);
	add_animation("knife_attack_right", 0, 15 * 64, 64, 64, 5, 0.1);

	// Bow attack
	add_animation("bow_attack_up", 0, 16 * 64, 64, 64, 13, 0.1);
	add_animation("bow_attack_left", 0, 17 * 64, 64, 64, 13, 0.1);
	add_animation("bow_attack_down", 0, 18 * 64, 64, 64, 13, 0.1);
	add_animation("bow_attack_right", 0, 19 * 64, 64, 64, 13, 0.1);

	add_animation("hurt", 0, 20 * 64, 64, 64, 3, 0.15);
	add_animation("die", 0, 20 * 64, 64, 64, 6, 0.15);
	add_animation("idle", 0, 18 * 64, 64, 64, 1, 1000);
	add_animation("dead", 5 * 64, 20 * 64, 64, 64, 1, 1000);
	play(m_default);
}

void AnimationPlayer::add_animation(std::string key, size_t x, size_t y,
									size_t width, size_t height, size_t nb_step,
									float step_time_s)
{
	if (has_animation(key))
	{
		Log::write_debug(
			"[AnimationPlayer]:\t Cannot add existing animation : " + key +
			".");
		return;
	}


	m_animations.insert(std::make_pair(
		key, Animation(x, y, nb_step, step_time_s, width, height)));
}

bool AnimationPlayer::has_animation(std::string key) const
{
	return m_animations.count(key) == 1;
}

bool AnimationPlayer::is_playing(std::string key)
{
	return has_animation(key) && m_animations[key].is_active();
}

void AnimationPlayer::play(std::string key)
{
	if (has_animation(key))
	{
		if (m_current_animation != key)
		{
			Log::write_debug("[AnimationPlayer]:\t Play animation " + key +
							 ".");
			m_animations[key].set_active(true);
			m_current_animation = key;
		}
	}
	else
	{
		Log::write_debug("[AnimationPlayer]:\t No animation " + key +
						 " stored.");
	}
}

void AnimationPlayer::update(GraphicalEntity& g, Occupier* o)
{
	if (!o->is_alive())
		m_default = "dead";
	// not working
	if (m_current_animation == "_uninitialized")
	{
		m_animations["walk_down"].update(g);
		return;
	}

	if (m_current_animation != "_none")
	{
		m_animations[m_current_animation].update(g);
		if (m_animations[m_current_animation].is_ended())
			play(m_default);

		if (!m_animations[m_current_animation].is_active())
		{
			// Log::write_debug("[AnimationPlayer]:\t Animation " +
			// 				 m_current_animation + " finished.");
			// m_current_animation = "_none";
		}
	}
}

bool AnimationPlayer::is_ended(std::string key)
{
	return !has_animation(key) || m_animations[key].is_ended();
}

AnimationPlayer::~AnimationPlayer(void)
{
}
