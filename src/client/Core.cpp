#include "client/Core.hpp"

#include <chrono>
#include <iostream>

#include "client/ConnectionScene.hpp"
#include "client/EndScene.hpp"
#include "client/GameScene.hpp"
#include "client/Globals.hpp"
#include "client/Input.hpp"
#include "client/LobbyScene.hpp"
#include "client/RoomScene.hpp"
#include "client/SplashScreenScene.hpp"
#include "client/settings.hpp"
#include "common/Log.hpp"


Core::Core(void)
	: m_graphic_system(m_resource_system)
	, m_scene_system(m_graphic_system, m_audio_system)
	, m_audio_system(m_resource_system)
{
	// Initialize the log
	Log::initialize("client");


	// Initialize the scenes
	m_scene_system.add_scene("splash", new SplashScreenScene);
	m_scene_system.add_scene("connection",
							 new ConnectionScene(m_network_system));
	m_scene_system.add_scene("lobby", new LobbyScene(m_network_system));
	m_scene_system.add_scene("room", new RoomScene(m_network_system));
	m_scene_system.add_scene("end", new EndScene(m_network_system));
	m_scene_system.add_scene(
		"game",
		new GameScene(m_network_system, m_graphic_system, m_audio_system));
	m_scene_system.set_current_scene("splash");
}

void Core::run(void)
{
	Log::write_debug("[Core]:\t Initialized, run the main loop.");
	std::chrono::time_point< std::chrono::steady_clock > last =
		std::chrono::steady_clock::now();
	float dt		   = 1.f;
	float elapsed_time = 0.f;
	// bool  print_time   = true;

	while (m_running)
	{
		// FPS Management
		std::chrono::time_point< std::chrono::steady_clock > now =
			std::chrono::steady_clock::now();
		std::chrono::duration< double > delta = now - last;

		dt   = delta.count();
		last = now;
		elapsed_time += dt;
		// std::this_thread::sleep_for(FRAMERATE - delta);

		// // Print fps
		// if ((int)(elapsed_time) % 2 == 0) // print every 2 seconds
		// {
		// 	if (print_time)
		// 		Log::write_debug("[Core]:\t Elapsed time " +
		// 						 std::to_string((int)elapsed_time) + " (FPS: " +
		// 						 std::to_string((int)(1.0 / dt)) + ")");
		// 	print_time = false;
		// }
		// else
		// 	print_time = true;
		// //

		m_graphic_system.update(dt); // Init GUI too

		m_scene_system.update(dt);
		m_scene_system.display();

		// Show FPS
		if (Input::is_action_pressed("debug"))
			Globals::get().monitoring = true;
		if (Input::is_action_pressed("ui_cancel"))
			Globals::get().monitoring = false;

		if (Globals::get().monitoring)
		{
			ImGui::SetNextWindowPos(sf::Vector2f(10, 20));
			ImGui::SetNextWindowSize(sf::Vector2f(240, 50));
			if (ImGui::Begin("Monitoring"))
			{
				ImGui::Text(
					("Loops = " + std::to_string((int)(1.0 / dt))).c_str());
				ImGui::End();
			}
		}

		// Last
		m_graphic_system.display();

		// End case
		if (m_graphic_system.is_ended())
		{
			m_running = false;
		}
	}
}

Core::~Core(void)
{
	Log::destroy();
}