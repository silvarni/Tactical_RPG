#include "client/Input.hpp"

#include "client/Globals.hpp"
#include "common/Log.hpp"

std::unique_ptr< Input > Input::m_input;
std::unordered_map< std::string, Control > Input::m_inputs;

Input::Input(void)
{
	m_input.reset(this);
}

void Input::initialize(void)
{
	new Input;
}


void Input::update(sf::Event event)
{

	// Events that are not about controls
	switch (event.type)
	{
	default: break;
	case sf::Event::LostFocus:
		// case sf::Event::MouseLeft:
		{
			Globals::get().has_focus = false;
		}
		break;

	case sf::Event::GainedFocus:
		// case sf::Event::MouseEntered:
		{
			Globals::get().has_focus = true;
		}
		break;
	}

	// No focus lead no control
	if (!Globals::get().has_focus)
		return;

	// Control events
	switch (event.type)
	{
	default: break;
	case sf::Event::KeyPressed:
	{
		switch (event.key.code)
		{
		default: break;
		case sf::Keyboard::Add: m_inputs["ui_plus"]		  = true; break;
		case sf::Keyboard::Subtract: m_inputs["ui_minus"] = true; break;
		case sf::Keyboard::Escape: m_inputs["ui_cancel"]  = true; break;
		case sf::Keyboard::Left: m_inputs["ui_left"]	  = true; break;
		case sf::Keyboard::Right: m_inputs["ui_right"]	= true; break;
		case sf::Keyboard::Down: m_inputs["ui_down"]	  = true; break;
		case sf::Keyboard::Up: m_inputs["ui_up"]		  = true; break;
		case sf::Keyboard::F3: m_inputs["debug"]		  = true; break;
		case sf::Keyboard::Space: m_inputs["focus"]		  = true; break;
		case sf::Keyboard::Tab: m_inputs["next_unit"]	 = true; break;
		}
	}
	break;

	case sf::Event::KeyReleased:
	{
		switch (event.key.code)
		{
		default: break;
		case sf::Keyboard::Add: m_inputs["ui_plus"]		  = false; break;
		case sf::Keyboard::Subtract: m_inputs["ui_minus"] = false; break;
		case sf::Keyboard::Escape: m_inputs["ui_cancel"]  = false; break;
		case sf::Keyboard::Left: m_inputs["ui_left"]	  = false; break;
		case sf::Keyboard::Right: m_inputs["ui_right"]	= false; break;
		case sf::Keyboard::Down: m_inputs["ui_down"]	  = false; break;
		case sf::Keyboard::Up: m_inputs["ui_up"]		  = false; break;
		case sf::Keyboard::F3: m_inputs["debug"]		  = false; break;
		case sf::Keyboard::Space: m_inputs["focus"]		  = false; break;
		case sf::Keyboard::Tab: m_inputs["next_unit"]	 = false; break;
		}
	}
	break;

	case sf::Event::MouseWheelMoved:
	{
		m_inputs["ui_wheel"].pressed		   = true;
		m_inputs["ui_wheel"].is_toggle_control = (event.mouseWheel.delta > 0);
	}
	break;

	case sf::Event::MouseButtonPressed:
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			m_inputs["mouse_left_pressed"]  = true;
			m_inputs["mouse_left_released"] = false;
		}
		if (event.mouseButton.button == sf::Mouse::Right)
		{
			m_inputs["mouse_right_pressed"]  = true;
			m_inputs["mouse_right_released"] = false;
		}
	}
	break;

	case sf::Event::MouseMoved:
	{
		if (m_inputs["mouse_left_pressed"])
			m_inputs["mouse_left_dragging"] = true;
		if (m_inputs["mouse_right_pressed"])
			m_inputs["mouse_right_dragging"] = true;
	}
	break;

	case sf::Event::MouseButtonReleased:
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			m_inputs["mouse_left_pressed"]  = false;
			m_inputs["mouse_left_released"] = true;
			m_inputs["mouse_left_dragging"] = false;
		}

		if (event.mouseButton.button == sf::Mouse::Right)
		{
			m_inputs["mouse_right_pressed"]  = false;
			m_inputs["mouse_right_dragging"] = false;
			m_inputs["mouse_right_released"] = true;
		}
	}
	break;
	}
}

Control& Input::is_action_pressed(std::string key)
{
	return m_inputs[key];
}

bool Input::is_any_key_pressed(void)
{
	for (auto& pair : m_inputs)
	{
		if (pair.first.find("mouse") == std::string::npos &&
			pair.second.pressed)
			return true;
	}
	return false;
}

Input::~Input(void)
{
	m_input.release();
}