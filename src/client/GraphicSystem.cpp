#include "client/GraphicSystem.hpp"

#include "client/Globals.hpp"
#include "client/Input.hpp"
#include "client/settings.hpp"
#include "common/Log.hpp"
#include "common/WorldData.hpp"

GraphicSystem::GraphicSystem(ResourceSystem& resource_system)
	: m_window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_TITLE,
			   sf::Style::Default)
	, m_vertex_array(sf::Quads)
	, m_resource_system(resource_system)
{
	Globals::get().current_window_size = m_window.getSize();
	m_window.setFramerateLimit(60);
	m_window.setVerticalSyncEnabled(false);

	// ImGui
	m_clock.restart();
	ImGui::SFML::Init(m_window);
	// ImGui::GetIO().DisplayFramebufferScale = ImVec2(1.5, 1.5);
	// ImGui::GetIO().FontGlobalScale = 2.0f;
	ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize,
						ImVec2(WINDOW_WIDTH / 10, WINDOW_HEIGHT / 10));
}

void GraphicSystem::update(float dt)
{
	ImGui::SFML::Update(m_clock.restart());
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		// Share it
		ImGui::SFML::ProcessEvent(event);
		Input::update(event);

		switch (event.type)
		{
		default: break;
		case sf::Event::Closed: m_window.close(); break;
		case sf::Event::Resized:
			Globals::get().current_window_size = m_window.getSize();
			break;
		case sf::Event::MouseMoved:
		{
			m_mouse_position =
				sf::Vector2i(event.mouseMove.x, event.mouseMove.y);
		}
		break;
		}
	}
	Globals::get().current_window_size = m_window.getSize();
}

void GraphicSystem::display(void)
{
	// Clear last
	clear();

	// Log::write_debug("[GraphicSystem]:\t Drawing " +
	// 				 std::to_string(m_entities.size()) + " entities and " +
	// 				 std::to_string(m_vertex_array.getVertexCount()) +
	// 				 " verteces.");

	// Start drawing the map
	m_window.draw(m_vertex_array, m_resource_system.get_texture("tileset"));
	if (m_resize)
	{
		// for (size_t i(m_resize); i < m_vertex_array.getVertexCount(); i++)
		// 	m_vertex_array[i].color = sf::Color::White;
		m_vertex_array.resize(m_resize);
		m_resize = 0;
	}

	// Now draw each element sorted by Z then by Y
	// Quite expensive !
	for (auto& entity : m_entities)
	{
		GraphicalEntity& g = const_cast< GraphicalEntity& >(
			entity); // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		// Why THE FUCK is the iterator of m_entities CONSTANT ?!!!!
		// ABSOLUTELY DISGUSTING SOLUTION, DO NOT REPRODUCE, DO NOT USE, DO NOT
		// SEE IT ! FUUUUUUUUUUUUCK (romain: haha)
		g.sprite.setTexture(*m_resource_system.get_texture(entity.texture_key));
		m_window.draw(g.sprite);
	}


	// Now the GUI
	ImGui::Render();

	// Display the whole thing
	m_window.display();

	// Clear contents
	m_entities.clear();
}

void GraphicSystem::set_vertex_array(sf::VertexArray& map)
{
	m_vertex_array.clear();
	m_vertex_array = map;
}

void GraphicSystem::clear(void)
{
	// m_window.clear(sf::Color(4, 139, 154)); // deep green
	m_window.clear(sf::Color(30, 30, 30));
}

void GraphicSystem::add_entity(const GraphicalEntity& entity)
{
	m_entities.insert(entity);
}

void GraphicSystem::add_entity(int x, int y, std::string texture_key,
							   int z_index)
{
	m_entities.emplace(x, y, texture_key, z_index);
}

void GraphicSystem::clear_contents(void)
{
	m_entities.clear();
	m_vertex_array.clear();
	set_view(m_window.getDefaultView());
}

ResourceSystem& GraphicSystem::get_resource_system(void)
{
	return m_resource_system;
}

sf::Vector2f GraphicSystem::get_global_mouse_position(void) const
{
	// return m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));
	return m_window.mapPixelToCoords(m_mouse_position);
}


sf::Vector2i GraphicSystem::get_mouse_position(void) const
{
	return m_mouse_position;
}

sf::Vector2f GraphicSystem::get_world_coordinates(sf::Vector2i v)
{
	return m_window.mapPixelToCoords(v);
}

GraphicSystem::~GraphicSystem(void)
{
	ImGui::SFML::Shutdown();
}