/**
 * \file main.cpp
 * \author Romain Mekarni Silvert Ervan
 */

#include "client/main.hpp"
#include "client/Core.hpp"

int main()
{
	Core core;
	core.run();
	return 0;
}
