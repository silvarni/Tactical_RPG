#include "client/RoomScene.hpp"

#include "client/Globals.hpp"
#include "client/settings.hpp"
#include "common/Log.hpp"
#include "common/Query.hpp"
#include "imgui.h"


RoomScene::RoomScene(NetworkSystem& network_system)
	: Scene("game")
	, m_network_system(network_system)
{
}

void RoomScene::update(float dt)
{
	Query query = m_network_system.get_received();
	if (!m_network_system.is_running())
	{
		m_network_system.disconnect();
		m_active	 = false;
		m_next_scene = "splash";
		return;
	}
	sf::Int8 action;
	if (!query.endOfPacket())
	{
		query >> action;
		Log::write_debug("[GameScene]:\t Treatment of action " +
						 Query::log(action) + ".");
		switch (action)
		{
		case DISCONNECT:
			m_network_system.disconnect();
			m_active	 = false;
			m_next_scene = "splash";
			break;
		case BACK:
			m_active	 = false;
			m_next_scene = "lobby";
			break;
		case TEAM:
		{
			sf::Int8 team;
			query >> team;
			Globals::get().team = team;
			Log::write_debug("[GameScene]:\t Set player team " +
							 std::to_string(Globals::get().team));
		}
		break;
		case TEAMS:
		{
			for (auto team : mm_players)
				for (auto player : team.second)
					delete player;
			mm_players.clear();
			std::string team;
			while (query >> team)
			{
				mm_players[team] = std::vector< char* >();
				sf::Int8 nb_players;
				query >> nb_players;
				for (int i = 0; i < nb_players; i++)
				{
					std::string name;
					query >> name;
					mm_players[team].push_back(strdup(name.data()));
				}
			}
		}
		break;
		case START:
			Log::write_debug("[RoomScene]\tStarting the game...");
			m_ready		 = false; // for restarting later
			m_active	 = false;
			m_next_scene = "game";
			break;
		case CHAT: m_chat.update(query); break;
		default: break;
		}
	}
}

void RoomScene::display(GraphicSystem& graphic_system,
						AudioSystem&   audio_system)
{
	// Music
	audio_system.play_music("tavern");

	// GUI
	ImGui::SetNextWindowPosCenter();
	ImGui::SetNextWindowSize(sf::Vector2u(700, 380));
	if (ImGui::Begin(std::string("Room : " + Globals::get().room_name).data(),
					 nullptr, DEFAULT_WINDOW_FLAG))
	{

		ImGui::Indent();
		ImGui::TextWrapped(
			std::string("Welcome Chief " + Globals::get().name).data());
		ImGui::TextWrapped(
			"The war shall begin when everyone is checked ready.");
		ImGui::Spacing();
		ImGui::Separator();
		ImGui::Spacing();
		ImGui::Columns(2);

		ImGui::BeginGroup();
		int i_team = 0;
		for (auto team : mm_players)
		{
			ImGui::TextWrapped(team.first.data());
			int selected = 0;
			ImGui::ListBox(team.first.data(), &selected,
						   (const char**)team.second.data(), team.second.size(),
						   5);
			if (!m_ready &&
				ImGui::Button(
					std::string("Join " + std::string(team.first.data()))
						.data(),
					DEFAULT_BUTTON_SIZE))
			{
				Query out;
				out << JOIN << (sf::Int8)i_team;
				m_network_system.send(out);
			}
			i_team++;
		}
		ImGui::EndGroup();

		ImGui::NextColumn();
		ImGui::BeginGroup();
		ImGui::Indent();
		ImGui::Spacing();
		if (ImGui::Checkbox("Sign your war announcement", &m_ready))
			m_network_system.send(READY);
		ImGui::Spacing();
		if (ImGui::Button("Flee and find another enemy",
						  DEFAULT_BUTTON_SIZE + sf::Vector2f(50.f, 0)))
		{
			m_network_system.send(BACK);
			m_active	 = false;
			m_next_scene = "lobby";
		}
		if (ImGui::Button("Back to Main menu",
						  DEFAULT_BUTTON_SIZE + sf::Vector2f(50.f, 0)))
			m_network_system.send(DISCONNECT);
		ImGui::EndGroup();

		ImGui::Spacing();
		ImGui::Indent();
		ImGui::Text("War meeting");
		Query query = m_chat.display();
		if (query.getDataSize())
			m_network_system.send(query);

		ImGui::End();
	}

	// SFML
	graphic_system.add_entity(0, 0, "splashscreen_background", -1);
}

void RoomScene::enter_tree()
{
	m_ready = false;
	if (Globals::get().team != -1)
	{
		Query out;
		out << JOIN << (sf::Int8)Globals::get().team;
		m_network_system.send(out);
	}
}

void RoomScene::exit_tree(void)
{
}

RoomScene::~RoomScene(void)
{
}
