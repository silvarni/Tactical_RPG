#include "client/SceneSystem.hpp"

#include <stdexcept>

#include "client/Scene.hpp"
#include "common/Log.hpp"

SceneSystem::SceneSystem(GraphicSystem& graphic_system,
						 AudioSystem&   audio_system)
	: m_graphic_system(graphic_system)
	, m_audio_system(audio_system)
{
}

void SceneSystem::update(float dt)
{
	if (m_current_scene)
	{
		// If unfinished
		if (m_current_scene->is_active())
			m_current_scene->update(dt);

		// if finished
		else
			set_current_scene(m_current_scene->get_next_scene());
	}
}

void SceneSystem::display(void)
{
	if (m_current_scene)
	{
		m_current_scene->display(m_graphic_system, m_audio_system);
	}
}

void SceneSystem::add_scene(std::string key, Scene* p_scene)
{
	// Security
	if (p_scene == nullptr)
	{
		throw std::invalid_argument(
			"[SceneSystem:]\t Can't add nullptr scene.'");
	}

	// If no scene yet, set this one as default
	if (m_current_scene == nullptr)
	{
		m_current_scene = p_scene;
	}


	m_scenes.insert(std::make_pair(key, p_scene));
}

#include <iostream>
void SceneSystem::set_current_scene(const std::string key,
									bool			  reinitialize_scene)
{
	// First of all, no artefact from past scene
	m_graphic_system.clear_contents();
	m_audio_system.clear();

	// Special case, the game is ended
	if (key == "exit")
	{
		m_graphic_system.close();
		return;
	}

	// Security
	if (m_scenes.find(key) == m_scenes.end())
	{
		throw std::invalid_argument("[SceneSystem]:\t Cannot 'set current "
									"scene'' with unexisting key : " +
									key + ".");
	}

	m_current_scene->exit_tree();
	m_current_scene = m_scenes[key].get();

	if (reinitialize_scene)
	{
		m_current_scene->enter_tree();
	}

	m_current_scene->set_active(true);

	Log::write_debug("[SceneSystem]:\t switched to scene " + key + ".");
}

SceneSystem::~SceneSystem(void)
{
}