#include <regex>
#include <string>


#include "client/Chat.hpp"
#include "client/Globals.hpp"
#include "client/GraphicSystem.hpp"
#include "common/Query.hpp"
#include "imgui.h"


Chat::Chat(void)
	: m_messages(MAX_CHAT_LINE_DISPLAYED, "")
{
	memset(&m_input, 0, sizeof(m_input));
	m_messages[MAX_CHAT_LINE_DISPLAYED - 2] =
		"[server] Welcome to Silvarni - Art of War";
	m_messages[MAX_CHAT_LINE_DISPLAYED - 1] =
		"(Type your message in the box below and press enter to chat.)";
}

void Chat::update(Query query)
{
	std::string message;
	query >> message;
	m_messages.push_back(message);
}

void Chat::update(std::string message)
{
	m_messages.push_back(message);
	if (m_messages.size() > 10)
		m_messages.pop_back();
}

Query Chat::display()
{
	Query query;
	// ImGui::SetNextWindowPos(
	// 	sf::Vector2f(
	// 		Globals::get().current_window_size.x - DEFAULT_WINDOW_SIZE.x,
	// 		Globals::get().current_window_size.y - DEFAULT_WINDOW_SIZE.y),
	// 	ImGuiSetCond_Always);
	// if (ImGui::Begin("Chat", nullptr, DEFAULT_WINDOW_FLAG))
	// {
	// 	ImGui::SetWindowSize(DEFAULT_WINDOW_SIZE);
	// 	ImGui::Indent();

	// ImGui::ListBox("Inn Talkings", &m_chat_input_selected,
	// 			   (const char**)m_messages.data(), m_messages.size(),
	// 			   MAX_CHAT_LINE_DISPLAYED);


	if (m_messages.size() <= MAX_CHAT_LINE_DISPLAYED)
	{
		for (auto& message : m_messages)
		{
			if (std::regex_search(message, std::regex("^\\[server\\]")))
				ImGui::TextColored(ImColor(255, 150, 150), message.c_str());
			else
				ImGui::TextWrapped(message.c_str());
		}
	}
	else
	{
		for (auto it = m_messages.end() - MAX_CHAT_LINE_DISPLAYED;
			 it != m_messages.end(); ++it)
		{
			if (std::regex_search(*it, std::regex("^\\[server\\]")))
				ImGui::TextColored(ImColor(255, 150, 150), it->c_str());
			else
				ImGui::TextWrapped(it->c_str());
		}
	}

	if (ImGui::InputText("###chat_it", m_input.data(), m_input.size(),
						 ImGuiInputTextFlags_EnterReturnsTrue |
							 ImGuiInputTextFlags_CtrlEnterForNewLine) &&
		m_input[0])
	{
		std::string message(m_input.data());
		query << CHAT << Globals::get().name + " : " + message;
		m_input.fill('\0');
	}
	// }
	// ImGui::End();
	return query;
}

Chat::~Chat(void)
{
}