#include "client/AudioSystem.hpp"
#include "common/Log.hpp"
#include <algorithm>

AudioSystem::AudioSystem(ResourceSystem& resource_system)
	: m_resource_system(resource_system)
{
}

void AudioSystem::play_music(std::string key, bool loop)
{
	if (key != m_current_music_key)
	{
		// Stop last
		if (m_current_music)
			m_current_music->stop();

		// Create new one
		m_current_music		= m_resource_system.get_music(key);
		m_current_music_key = key;
		m_current_music->play();
		m_is_playing = true;
		m_current_music->setLoop(loop);
	}
}

void AudioSystem::clear(void)
{
	if (m_current_music)
		m_current_music->pause();
	m_sounds.clear();
}

void AudioSystem::pause(void)
{
	m_current_music->pause();
}

void AudioSystem::play_sound(std::string key, sf::Vector2f position)
{
	sf::SoundBuffer* buffer = m_resource_system.get_sound(key);
	if (buffer)
	{
		m_sounds.emplace_back(*buffer);
		m_sounds.back().setPosition(sf::Vector3f(position.x, position.y, 0));
		m_sounds.back().play();
		Log::write_debug("[AudioSystem]:\t Play " + key + " sound.");
	}

	if (m_sounds.size() >= 50)
	{
		std::remove_if(m_sounds.begin(), m_sounds.end(),
					   [](sf::Sound& s) { return s.getStatus() == 0; });
	}
}

AudioSystem::~AudioSystem(void)
{
}