#include "client/ConnectionScene.hpp"

#include "client/settings.hpp"
#include "common/Log.hpp"
#include "common/Query.hpp"
#include "imgui.h"

ConnectionScene::ConnectionScene(NetworkSystem& network_system)
	: Scene("lobby")
	, m_network_system(network_system)
{
}

void ConnectionScene::update(float dt)
{
}

void ConnectionScene::display(GraphicSystem& graphic_system,
							  AudioSystem&   audio_system)
{
	// GUI
	ImGui::SetNextWindowPosCenter();
	if (ImGui::Begin("Connection", nullptr, DEFAULT_WINDOW_FLAG))
	{
		ImGui::SetWindowSize(DEFAULT_WINDOW_SIZE);
		ImGui::Indent();

		if (m_connected)
		{
			if (m_connecting && m_network_system.is_connecting())
			{
				ImGui::Text("Finding the war council.");
				if (ImGui::Button("Cancel", DEFAULT_BUTTON_SIZE))
					m_network_system.interrupt();
			}
			else // NetworkManager is no longer connecting
			{
				m_connecting = false;
				// Look for result,
				// Herer the connection is done
				if (m_connected &&
					m_network_system.is_connected()) // Carreful, is_connected
													 // returns true while
													 // connecting
				{
					audio_system.play_sound("door_close");
					m_next_scene = "lobby";
					m_active	 = false;
				}
				m_connected = false;
			}
		}
		else
		{
			// Enter Adresse and Port
			if (m_has_tried_connection)
				ImGui::TextWrapped("You couldn't find the war council in "
								   "the deep forest.");

			ImGui::Text("Server IP Address :");
			ImGui::SameLine();
			ImGui::InputText("###ip", m_ip_address, 255, 0);

			ImGui::Text("Server Port       :");
			ImGui::SameLine();

			ImGui::InputInt("###port", &m_port, 1);
			if (ImGui::Button("Connect") && !m_connecting)
			{
				m_network_system.initialize(m_ip_address, m_port);
				m_connecting		   = true;
				m_connected			   = true;
				m_has_tried_connection = true;
			}
			if (ImGui::Button("Back To Main Menu", DEFAULT_BUTTON_SIZE))
			{
				m_next_scene = "splash";
				m_active	 = false;
			}
		}
		ImGui::End();
	}
	// SFML
	graphic_system.add_entity(0, 0, "splashscreen_background", -1);
}

void ConnectionScene::enter_tree(void)
{
	m_connecting		   = false;
	m_connected			   = false;
	m_has_tried_connection = false;
}

void ConnectionScene::exit_tree(void)
{
}

ConnectionScene::~ConnectionScene(void)
{
}
