#include "client/ResourceSystem.hpp"
#include "common/Log.hpp"

ResourceSystem::ResourceSystem(void)
{
	m_textures.insert(std::make_pair("NONE", sf::Texture()));
	load_texture("ERROR");

	// ===== ADD ALL THE COORDINATES =====
	add_texture_coordinates("ERROR", sf::Vector2f(0, 0), sf::Vector2f(32, 32));
	add_texture_coordinates("cell_empty", sf::Vector2f(32, 0),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_spawn", sf::Vector2f(32 * 2, 32),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_rock", sf::Vector2f(32 * 2, 0),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_water", sf::Vector2f(32 * 3, 0),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_tree", sf::Vector2f(32 * 4, 0),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_house", sf::Vector2f(32 * 5, 0),
							sf::Vector2f(32, 32));


	add_texture_coordinates("cell_contour", sf::Vector2f(0, 32),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_hover", sf::Vector2f(32 * 1, 32),
							sf::Vector2f(32, 32));
	add_texture_coordinates("cell_selected", sf::Vector2f(32 * 2, 32),
							sf::Vector2f(32, 32));
}

void ResourceSystem::add_texture_coordinates(std::string key, sf::Vector2f pos,
											 sf::Vector2f size)
{
	m_texture_coordinates.insert(std::make_pair(key + "_0", pos));
	m_texture_coordinates.insert(
		std::make_pair(key + "_1", pos + sf::Vector2f(size.x, 0)));
	m_texture_coordinates.insert(std::make_pair(key + "_2", pos + size));
	m_texture_coordinates.insert(
		std::make_pair(key + "_3", pos + sf::Vector2f(0, size.y)));
}

sf::Texture* ResourceSystem::get_texture(std::string key)
{
	if (m_textures.find(key) != m_textures.end() || load_texture(key))
	{
		return &m_textures[key];
	}
	else
	{
		// Problem
		Log::write_error("[ResourceSystem]:\t Cannot find texture " + key +
						 ". (Use ERROR instead)");
		m_textures[key] = m_textures["ERROR"];
		return &m_textures[key];
	}
}

sf::Vector2f ResourceSystem::get_texture_coordinates(std::string key,
													 size_t		 index)
{
	std::string full_key = key + "_" + std::to_string(index);
	// Making sure it exists is too greedy (3 times longer)
	// if (m_texture_coordinates.find(full_key) == m_texture_coordinates.end())
	// {
	// 	Log::write_debug("[ResourceSystem]:\t Cannot find coordinates for id " +
	// 					 key + ".");
	// 	m_texture_coordinates[full_key] =
	// 		get_texture_coordinates("ERROR", index);
	// }
	return m_texture_coordinates[full_key];
}

bool ResourceSystem::load_texture(std::string key)
{
	sf::Texture texture;
	if (!texture.loadFromFile(ASSET_DIR + "textures/" + key + ".png"))
		return false;

	m_textures.insert(std::make_pair(key, texture));

	return true;
}


sf::Music* ResourceSystem::get_music(std::string key)
{
	if (m_musics.find(key) != m_musics.end() || load_music(key))
	{
		return &m_musics[key];
	}
	else
	{
		// Problem
		Log::write_error("[ResourceSystem]:\t Cannot find music " + key + ".");
		return nullptr;
	}
}

sf::SoundBuffer* ResourceSystem::get_sound(std::string key)
{
	if (m_sound_buffers.find(key) != m_sound_buffers.end() || load_sound(key))
	{
		return &m_sound_buffers[key];
	}
	else
	{
		// Problem
		Log::write_error("[ResourceSystem]:\t Cannot find sound " + key + ".");
		return nullptr;
	}
}


bool ResourceSystem::load_music(std::string& key)
{
	sf::Music& music = m_musics[key];
	if (!music.openFromFile(ASSET_DIR + "musics/" + key + ".ogg"))
		return false;

	return true;
}

bool ResourceSystem::load_sound(std::string key)
{
	sf::SoundBuffer& buffer = m_sound_buffers[key];
	if (!buffer.loadFromFile(ASSET_DIR + "sounds/" + key + ".ogg"))
		return false;

	return true;
}


ResourceSystem::~ResourceSystem(void)
{
}