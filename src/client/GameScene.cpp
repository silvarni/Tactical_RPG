#include "client/GameScene.hpp"

#include <array>
#include <chrono>


#include "client/Globals.hpp"
#include "common/OccupierFactory.hpp"
#include "imgui.h"

GameScene::GameScene(NetworkSystem& network_system,
					 GraphicSystem& graphic_system, AudioSystem& audio_system)
	: Scene("UNSET")
	, m_world_view(m_world, graphic_system, network_system, audio_system)
	, m_network_system(network_system)
{
}

void GameScene::update(float dt)
{
	Query query = m_network_system.get_received();
	if (!m_network_system.is_running())
	{
		m_network_system.disconnect();
		m_active	 = false;
		m_next_scene = "splash";
		return;
	}
	sf::Int8 action;
	while (!query.endOfPacket())
	{
		query >> action;
		Log::write_debug("[GameScene]:\t Treatment of action " +
						 Query::log(action) +
						 "."); // TODO: Move into Log::to_string(action)
		switch (action)
		{
		case DISCONNECT:
			m_network_system.disconnect();
			m_active	 = false;
			m_next_scene = "splash";
			break;
		case BACK:
			Globals::get().turn = false;
			m_active			= false;
			m_next_scene		= "room";
			break;
		case WORLD: query >> m_world; break;
		case INIT_WORLD:
			query >> m_world;
			m_world_view.reinitialize();
			break;
		case TURN:
		{
			if (m_positioning && Globals::get().turn)
			{
				m_positioning			   = false;
				Globals::get().positioning = false;
				m_world_view.get_view_manager().set_auto_focus(true);
			}
			sf::Int8 turn_time;
			query >> turn_time;
			Globals::get().turn = !Globals::get().turn;
			if (Globals::get().turn)
			{
				Globals::get().turn_time	  = turn_time;
				Globals::get().turn_time_left = Globals::get().turn_time;
				m_world.new_turn();
				m_world_view.new_turn();
			}
			break;
		}
		case SPAWNS:
		{
			Globals::get().positioning = true;
			m_positioning			   = true;
			sf::Vector2u pos;
			while (query >> pos)
			{
				m_world_view.add_spawn(pos);
			}
			m_world_view.set_view_center(pos.x * CELL_WIDTH,
										 pos.y * CELL_HEIGHT);
			break;
		}
		case GOLD:
		{
			Log::write_debug("Gold was " + std::to_string(Globals::get().gold));
			sf::Int16 gold;
			query >> gold;
			Globals::get().gold = (int)gold;
			Log::write_debug("Gold is now " +
							 std::to_string(Globals::get().gold));
			break;
		}
		case END:
		{
			sf::Int8 winner;
			query >> winner;
			Globals::get().win = ((int)winner == Globals::get().team);
			m_active		   = false;
			m_next_scene	   = "end";
			break;
		}
		case CHAT:
			m_chat.update(query);
			query.clear();
			break;
		default: break;
		}
	}
	m_world.update(dt);
	m_world_view.update(dt);
	Globals::get().turn_time_left -= dt;
}

void GameScene::quit(void)
{
	Log::write_debug("[GameScene]:\t Send request to disconnect...");
}

void GameScene::display(GraphicSystem& graphic_system,
						AudioSystem&   audio_system)
{
	// Music
	audio_system.play_music("war");

	if (m_world.is_updated())
	{
		// graphic_system.clear_contents();
		m_world_view.reinitialize_occupiers(); // recreate every occupier
	}

	// Menu bar
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("Return"))
		{
			if (ImGui::Button("Leave game"))
			{
				m_network_system.send(BACK);
				quit();
			}
			if (ImGui::Button("Quit game"))
			{
				m_network_system.send(DISCONNECT);
				quit();
			}
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Setting"))
		{
			// Mouse scrolling
			ImGui::Checkbox("Enable mouse scrolling",
							&Globals::get().mouse_scroll_enabled);

			// Autofocus
			ImGui::Checkbox("Enable auto-focus",
							&Globals::get().auto_focus_enabled);
			ImGui::EndMenu();
		}


		if (ImGui::BeginMenu(Globals::get().room_name.data()))
			ImGui::EndMenu();
		if (ImGui::BeginMenu(Globals::get().name.data()))
			ImGui::EndMenu();
		if (ImGui::BeginMenu(
				std::string("Team " + std::to_string(Globals::get().team))
					.data()))
			ImGui::EndMenu();
		ImGui::TextWrapped(
			("Gold : " + std::to_string(Globals::get().gold)).c_str());

		ImGui::EndMainMenuBar();
	}

	// War Panel
	ImGui::SetNextWindowPos(
		sf::Vector2u(0, Globals::get().current_window_size.y - 200));
	ImGui::SetNextWindowSize(
		sf::Vector2u(Globals::get().current_window_size.x, 200));
	if (ImGui::Begin("War Panel", nullptr, DEFAULT_WINDOW_FLAG))
	{
		ImGui::Indent();
		ImGui::Spacing();
		ImGui::Columns(3);
		m_world_view.print_current_actor_information();
		ImGui::NextColumn();

		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		if (m_positioning &&
			ImGui::Checkbox("Ready to fight !", &m_checkbox_positioning))
		{
			m_network_system.send(POSITIONING);
			Globals::get().positioning = !m_checkbox_positioning;
		}
		else if (!m_positioning && Globals::get().turn &&
				 ImGui::Checkbox("End turn", &m_checkbox_endturn))
		{
			m_network_system.send(POSITIONING);
			m_checkbox_endturn = false;
		}

		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::BeginGroup();
		if (Globals::get().turn)
		{
			ImGui::ProgressBar(
				1 - Globals::get().turn_time_left / Globals::get().turn_time,
				sf::Vector2f(200.f, 0.0f),
				std::to_string((int)(Globals::get().turn_time_left)).data());
		}
		else
		{
			ImGui::TextWrapped("Please wait for the enemy to play.");
		}
		ImGui::EndGroup();

		ImGui::NextColumn();
		ImGui::BeginGroup();
		ImGui::Text("War letters");
		Query query = m_chat.display();
		if (query.getDataSize())
			m_network_system.send(query);
		ImGui::EndGroup();

		ImGui::End();
	}
	m_world_view.display();
}

void GameScene::enter_tree(void)
{
	Log::write_debug("[GameScene]:\t Reinitialized");
	m_world.reinitialize();
	m_world_view.reinitialize();
	Globals::get().positioning = true;
	m_positioning			   = true;
	m_checkbox_positioning	 = false;
	Globals::get().positioning = false;
}

void GameScene::exit_tree(void)
{
	m_world_view.clear();
}

void GameScene::set_enemi_name(Query query)
{
}

GameScene::~GameScene(void)
{
	m_network_system.send(DISCONNECT);
}