#include "common/Log.hpp"

std::unique_ptr< Log >								 Log::m_self;
std::ofstream										 Log::m_output;
std::string											 Log::m_target;
std::chrono::time_point< std::chrono::steady_clock > Log::m_start =
	std::chrono::steady_clock::now();

Log::Log(void)
{
	if (m_self == nullptr)
	{
		m_self.reset(this);
	}
}

void Log::initialize(std::string output_name)
{
	if (m_self == nullptr)
	{
		new Log;
		m_target = output_name;
		m_output.open(output_name + ".log");
	}
}

void Log::write(std::string message, SEVERITY severity)
{
	if (severity < m_level)
		return;

	std::string severity_msg = "";

	switch (severity)
	{
	default: break;
	case SEVERITY::LOW: severity_msg	  = "[DEBUG] :\t"; break;
	case SEVERITY::MEDIUM: severity_msg   = "[INFO] :\t"; break;
	case SEVERITY::HIGHT: severity_msg	= "[WARNING] :\t"; break;
	case SEVERITY::CRITICAL: severity_msg = "[CRITICAL] :\t"; break;
	case SEVERITY::ERROR: severity_msg	= "[ERROR]  :\t"; break;
	}

	m_output << (std::chrono::steady_clock::now() - m_start).count() / 1e9
			 << "s :\t" << m_target << " -- " << severity_msg << message;
	std::cerr << (std::chrono::steady_clock::now() - m_start).count() / 1e9
			  << "s :\t" << m_target << " --" << severity_msg << message;
}

void Log::write_error(std::string message)
{
	write(message + "\n", SEVERITY::ERROR);
}

void Log::write_debug(std::string message)
{
	write(message + "\n", SEVERITY::LOW);
}

void Log::destroy(void)
{
	m_self.release();
}

Log::~Log(void)
{
	m_output.close();
}
