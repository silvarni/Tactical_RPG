#include "common/Perlin.hpp"

#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <random>

Perlin::Perlin(size_t w, size_t h, size_t step, bool autoRandom)
	: m_step(step)
	, m_width(w)
	, m_height(h)
	, m_tab(m_height, std::vector< double >(m_width, 0.f))
{
	initTab(); // Puts values to a default value, more often is 0
	if (autoRandom)
		randomize(); // Puts random values at regular steps
	interpolate();   // Interpolates all the cells
}

void Perlin::reset(void)
{
	initTab();	 // Puts values to a default value, more often is 0
	randomize();   // Puts random values at regular steps
	interpolate(); // Interpolates all the cells
}

void Perlin::initTab()
{
	for (size_t i(0); i < m_height; i++)
		for (size_t j(0); j < m_width; j++)
			m_tab.at(i).at(j) = 0.f;
}

void Perlin::randomize()
{
	std::default_random_engine generator(
		std::chrono::steady_clock::now().time_since_epoch().count());
	std::uniform_real_distribution< double > distrib(0, 100);
	// std::normal_distribution<double> distrib(0,100);
	// std::lognormal_distribution<double> distrib(0,2);
	// std::chi_squared_distribution<double> distrib(40);
	// std::fisher_f_distribution<double> distrib(1.0,1.0);
	// std::discrete_distribution<int>
	// distrib{0,10,20,30,40,50,60,70,80,90,100,100,100,100};


	// create the rect values
	for (size_t i(0); i < m_height; i++)
		for (size_t j(0); j < m_width; j++)
		{
			if ((i % m_step == 0 || i == m_height - 1) &&
				(j % m_step == 0 || j == m_width - 1))
				m_tab.at(i).at(j) = distrib(generator);
		}
}

double Perlin::linear_interpolation(double a, double b, double t) const
{
	return a * (1.0 - t) + b * t;
}

double Perlin::polynomial_interpolation(double a, double b, double t) const
{
	t *= 3.141592653589; // PI
	double u = (1 - cos(t)) * .5;
	return linear_interpolation(a, b, u);
}


bool Perlin::inMap(size_t i, size_t j) const
{
	return (i < m_height && j < m_width); // > 0 is useless when using size_t
}

void Perlin::interpolate()
{
	size_t Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, Fx, Fy;

	for (size_t i(0); i < m_width; i++)
	{
		for (size_t j(0); j < m_height; j++)
		{
			if (i % m_step == 0 && i < m_width)
			{
				Ay = i;
			}
			if (j % m_step == 0 && j < m_height)
			{
				Ax = j;
			}

			/* using std::min in order to interpolate to the end */

			By = Ay;
			Bx = std::min(Ax + m_step, m_width - 1);

			Cx = std::min(Ax + m_step, m_width - 1);
			Cy = std::min(Ay + m_step, m_height - 1);

			Dx = Ax;
			Dy = std::min(Ay + m_step, m_width - 1);

			Ex = j;
			Ey = Ay;

			Fx = j;
			Fy = Dy;

			// Perlin
			if (inMap(Cx, Cy))
			{
				m_tab.at(Ex).at(Ey) = polynomial_interpolation(
					m_tab.at(Ax).at(Ay), m_tab.at(Bx).at(By),
					(double)(Ex - Ax) / (double)(Bx - Ax));
				m_tab.at(Fx).at(Fy) = polynomial_interpolation(
					m_tab.at(Dx).at(Dy), m_tab.at(Cx).at(Cy),
					(double)(Fx - Dx) / (double)(Cx - Dx));
				m_tab.at(j).at(i) = polynomial_interpolation(
					m_tab.at(Ex).at(Ey), m_tab.at(Fx).at(Fy),
					(double)(i - Ey) / (double)(Fy - Ey));
			}
		}
	}
}

size_t Perlin::random(size_t min, size_t max) const
{
	return rand() % max + min;
}

void Perlin::display()
{
	for (size_t i(0); i < m_height; i++)
	{
		for (size_t j(0); j < m_width; j++)
		{
			std::cerr << m_tab[i][j] << " ";
		}
		std::cerr << '\n';
	}
}

/* =============== GETTER =============== */
const double Perlin::get(size_t x, size_t y) const
{
	assert(y < m_height && x < m_width);
	return m_tab.at(y).at(x);
}

const size_t Perlin::getWidth() const
{
	return m_width;
}

const size_t Perlin::getHeight() const
{
	return m_height;
}

Perlin::~Perlin()
{
	// dtor
}
