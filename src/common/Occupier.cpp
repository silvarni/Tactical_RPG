/**
 * \file Occupier.cpp
 * \author Romain Mekarni
 */

#include "common/Occupier.hpp"
#include "common/Query.hpp"
#include "common/WorldData.hpp"

Occupier::Occupier(sf::Vector2u pos, std::string name, std::string weapon_name,
				   int team, float damage, float max_health, int speed,
				   int range, DIRECTION direction)
	: m_position(pos)
	, m_name(name)
	, m_weapon_name(weapon_name)
	, m_team(team)
	, m_damage(damage)
	, m_max_health(max_health)
	, m_health(max_health)
	, m_speed(speed)
	, m_range(range)
	, m_direction(direction)
{
}

void Occupier::update(float dt, WorldData& world)
{
	// Unoccupy
	world.set_occupier(m_position, nullptr);
	if (is_moving())
		m_action_time += dt;
	if (is_moving() && m_action_time >= TIME_TO_TRAVEL_ONE_CELL_S)
	{

		switch (m_direction)
		{
		case NORTH: m_position.y--; break;
		case SOUTH: m_position.y++; break;
		case EAST: m_position.x++; break;
		case WEST: m_position.x--; break;
		}
		path.pop_front();
		if (!path.empty())
			m_direction = path.front();
		else
			m_action = ACTION::IDLE;

		m_action_time -= TIME_TO_TRAVEL_ONE_CELL_S;
		if (!m_positioning)
			m_steps++;
		update_range(world);
		// std::cout << " m_direction " << m_direction << std::endl;
		// std::cout << " top path is  " << path.front()
		// 		  << " action : " << m_action << std::endl;
		// Log::write_debug("nouvelle position : " +
		// Log::to_string(m_position));
	}

	// Occupy
	world.set_occupier(m_position, this);
}

void Occupier::update(WorldData& world)
{
	world.set_occupier(m_position, nullptr);
	while (is_moving())
	{
		switch (m_direction)
		{
		case NORTH: m_position.y--; break;
		case SOUTH: m_position.y++; break;
		case EAST: m_position.x++; break;
		case WEST: m_position.x--; break;
		}
		path.pop_front();
		if (!path.empty())
			m_direction = path.front();
		else
			m_action = ACTION::IDLE;
		if (!m_positioning)
			m_steps++;
	}
	world.set_occupier(m_position, this);
}

void Occupier::update_range(const WorldData& world, int max)
{
	move_range.clear();
	attack_range.clear();
	max = std::min((int)m_speed, max);
	std::vector< const Cell* > last_loop;
	last_loop.push_back(&world.get_cell(m_position.x, m_position.y));
	for (int step = m_steps; step < max + m_range; ++step)
	{
		std::vector< const Cell* > temp;
		temp.swap(last_loop);

		for (auto& cell : temp)
		{
			move_range.push_back(cell);
			auto neighboors = world.get_neighboor_cells(cell->x, cell->y);
			for (auto& c : neighboors)
			{
				// Verify not already in the range
				if (std::find(move_range.begin(), move_range.end(), c) !=
					std::end(move_range))
				{
					continue;
				}

				if (can_walk_on(c->m_type) && !c->m_occupier && step + 1 <= max)
				{
					move_range.push_back(c);
					last_loop.push_back(c);
				}
				else
					attack_range.push_back(c);
			}
		}
	}
}


#include <unordered_map>
bool Occupier::go_to(size_t x, size_t y, WorldData& world,
					 bool to_occupied_cell)
/**
* A* algorithm, following RedBlobGames tutorial
* link :
* http://www.redblobgames.com/pathfinding/a-star/implementation.html#orgheadline18
*/
{
	update_range(world);

	const Cell* cell = &world.get_cell(x, y);

	// Don't calculate if can't move
	if (m_steps >= m_speed || (!to_occupied_cell &&
							   std::find(move_range.begin(), move_range.end(),
										 cell) == std::end(move_range) &&
							   !m_positioning))
		return false;

	const Cell* goal  = &world.get_cell(x, y);
	const Cell* start = &world.get_cell(m_position);

	astar_queue_t priority_queue(astar_compare_f);
	// priority_queue.push( astar_node_t( &world.get_cell(m_position.x,
	// m_position.y), 0));
	priority_queue.push(
		astar_node_t(start, std::abs((int)(goal->x - m_position.x)) +
								std::abs((int)(goal->y - m_position.y))));

	std::unordered_map< const Cell*, int > cost_so_far;
	cost_so_far[start] = 0;
	std::unordered_map< const Cell*, const Cell* > came_from;
	came_from[start] = start;

	// Empty if impossible to reach
	while (!priority_queue.empty())
	{
		const Cell* current = priority_queue.top().first;
		if (current == goal)
			break;
		std::vector< const Cell* > neighboors =
			world.get_neighboor_cells(current->x, current->y);

		for (auto& p_cell : neighboors)
		{
			if (!can_walk_on(p_cell->m_type) ||
				(p_cell->m_occupier && !to_occupied_cell) ||
				(p_cell->m_occupier && to_occupied_cell && p_cell != goal))
				continue;

			int dx = p_cell->x - current->x;
			int dy = p_cell->y - current->y;

			int cost = cost_so_far[current] + std::abs(dx) + std::abs(dy);
			if (cost_so_far.find(p_cell) == cost_so_far.end() ||
				cost < cost_so_far[p_cell])
			{
				cost_so_far[p_cell] = cost;
				// Log::write_debug("[Occupier::PathFinding]:\t Is in " +
				// std::to_string(m_position.x) + " " +
				// std::to_string(m_position.y)
				// + " testing " + std::to_string(current->x) + " " +
				// std::to_string(current->y) + " to "
				// + std::to_string(p_cell->x) + " " +
				// std::to_string(p_cell->y)
				// + " is heurisitic " + std::to_string(heuristic(goal->x,
				// goal->y, p_cell->x, p_cell->y)));
				int heuristic =
					abs(goal->x - p_cell->x) +
					abs(goal->y - p_cell->y); //+ abs(p_cell->x - m_position.x)
											  //+ abs(p_cell->y - m_position.y);
				priority_queue.push(astar_node_t(p_cell, cost + heuristic));
				came_from[p_cell] = current;
			}
		}
		if (priority_queue.top().first == current)
			priority_queue.pop(); // avoid some rare loops
	}

	// Now actually getting the path
	path.clear();

	const Cell* current = goal;
	const Cell* next	= nullptr;

	do
	{
		next	= current;
		current = came_from[current];
		if (current == nullptr)
			return false;

		// TODO: check for any obstacle or effects

		// assert(next);
		// assert(current);
		int dx = next->x - current->x;
		int dy = next->y - current->y;
		if (dx == -1)
			path.push_front(WEST);
		else if (dx == 1)
			path.push_front(EAST);
		else if (dy == -1)
			path.push_front(NORTH);
		else if (dy == 1)
			path.push_front(SOUTH);
	} while (current != next && current); // As came from start = start

	if (to_occupied_cell && !path.empty())
		path.pop_back(); // remove occupied cell
	m_action = (path.empty()) ? ACTION::IDLE : ACTION::MOVING;
	return true;
}

bool Occupier::can_walk_on(TYPE t)
{
	return t == EMPTY;
}

void Occupier::attack(Occupier* enemy)
{
	if (enemy && !m_has_attacked)
	{
		m_target	   = enemy->get_position();
		m_has_attacked = true;
		enemy->suffer(m_damage);
		m_action = (path.empty()) ? ACTION::ATTACKING : ACTION::MOVE_AND_ATTACK;
		// TODO: Point d'action'
		// En attendant, fin de tour
		// m_steps = m_speed;
	}
}

void Occupier::suffer(float damage)
{
	m_health -= damage;
	if (!is_alive())
		set_action(ACTION::DIE);
	else
		m_action = ACTION::ATTACKED;
}

void Occupier::new_turn(void)
{
	m_steps		   = 0;
	m_has_attacked = false;
	m_action	   = IDLE;
}

std::string Occupier::get_str_direction(void) const
{
	switch (m_direction)
	{
	case NORTH: return "up"; break;
	case WEST: return "left"; break;
	case EAST: return "right"; break;
	case SOUTH: return "down"; break;
	}
	return "no_direction";
}

Occupier::~Occupier(void)
{
}
