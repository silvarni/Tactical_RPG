#include "common/Cell.hpp"


Cell::Cell(TYPE type)
	: m_type(type)
{
}

Cell::Cell(size_t x, size_t y, TYPE type)
	: x(x)
	, y(y)
	, m_type(type)
{
}

std::string Cell::get_visual_name(void) const
{
	switch (m_type)
	{
	default: return "Unknown"; break;
	case EMPTY: return "Grass"; break;
	case TREE: return "Tree"; break;
	case ROCK: return "Rock"; break;
	case WATER: return "Water"; break;
	}
}
