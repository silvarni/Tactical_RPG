/**
 * \file Query.cpp
 * \author Romain Mekarni
 */

#include "common/Query.hpp"
#include "common/Occupier.hpp"
#include "common/WorldData.hpp"


Query::Query(void)
{
}

sf::Packet& operator<<(sf::Packet& packet, const sf::Vector2u in)
{
	return packet << in.x << in.y;
}

sf::Packet& operator>>(sf::Packet& packet, sf::Vector2u& in)
{
	return packet >> in.x >> in.y;
}

sf::Packet& operator<<(sf::Packet& packet, const Cell& cell)
{
	if (cell.m_type != EMPTY || cell.m_occupier || cell.m_updated)
	{
		packet << sf::Vector2u(cell.x, cell.y) << cell.m_type
			   << !(cell.m_occupier == nullptr);
		if (cell.m_occupier)
			packet << *(cell.m_occupier);
	}
	return packet;
}

bool operator>>(sf::Packet& packet, Cell& cell)
{
	bool occupied;
	packet >> cell.m_type >> occupied;
	return occupied; // occupier is refresh in Query>>WorldData
}

sf::Packet& operator<<(sf::Packet& packet, const Occupier& occupier)
{
	packet << occupier.m_target << occupier.m_name << occupier.m_weapon_name
		   << occupier.m_team << occupier.m_damage << occupier.m_max_health
		   << occupier.m_health << occupier.m_speed << occupier.m_range
		   << occupier.m_direction << occupier.m_action
		   << occupier.m_positioning;
	if (occupier.is_moving())
	{
		packet << (sf::Int8)occupier.path.size();
		for (size_t i = 0; i < occupier.path.size(); i++)
			packet << occupier.path[i];
	}
	return packet;
}

sf::Packet& operator>>(sf::Packet& packet, Occupier& occupier)
{
	packet >> occupier.m_target >> occupier.m_name >> occupier.m_weapon_name >>
		occupier.m_team >> occupier.m_damage >> occupier.m_max_health >>
		occupier.m_health >> occupier.m_speed >> occupier.m_range >>
		occupier.m_direction >> occupier.m_action >> occupier.m_positioning;
	if (occupier.is_moving()) // || occupier.m_action == MOVE_AND_ATTACK
	{
		occupier.path.clear();
		sf::Int8 n, direction;
		packet >> n;
		for (int i = 0; i < n; i++)
		{
			packet >> direction;
			occupier.path.push_back((DIRECTION)direction);
		}
		occupier.m_action_time = 0.f;
		occupier.m_direction   = occupier.path.front();
		Log::write_debug("[WorldData]:\t Occupier " +
						 Log::to_string(occupier.m_position) + " is MOVING");
	}
	return packet;
}

sf::Packet& operator<<(sf::Packet& packet, const WorldData& world)
{
	for (size_t i = 0; i < WORLD_HEIGHT; i++)
		for (size_t j = 0; j < WORLD_WIDTH; j++)
			packet << world.get_cell(j, i);
	return packet;
}

sf::Packet& operator>>(sf::Packet& packet, WorldData& world)
{
	sf::Vector2u pos;
	while (packet >> pos) // each new pos = new cell to update
	{
		Log::write_debug("[WorldData]\t Refresh " + Log::to_string(pos));
		if (packet >> world.get_cell(pos)) // if there is an occupier
		{
			// Create new occupier ?
			if (!world.get_occupier(pos))
				world.add_occupier(new Occupier(pos));

			// Update anyway
			packet >> *(world.get_occupier(pos));
			auto& occupier = *(world.get_occupier(pos));

			world.actions.emplace_back((ACTION)occupier.get_action(), pos,
									   &occupier);
			// Log::write_debug("[Query]:\t Added action : " +
			// 				 std::to_string(occupier.get_action()));
		}
		else if (world.get_cell(pos).m_occupier)
			world.remove_occupier(world.get_cell(pos).m_occupier);
	}
	return packet;
}


Query::~Query(void)
{
}

std::string Query::log(sf::Int8 action)
{
	switch (action)
	{
	case WORLD: return std::string("WORLD");
	case TURN: return std::string("TURN");
	case MOVE: return std::string("MOVE");
	case ATTACK: return std::string("ATTACK");
	case NAME: return std::string("NAME");
	case CHAT: return std::string("CHAT");
	case NOTHING: return std::string("NOTHING");
	case DISCONNECT: return std::string("DISCONNECT");
	case READY: return std::string("READY");
	case JOIN: return std::string("JOIN");
	case FULL: return std::string("FULL");
	case CLIENTS: return std::string("CLIENTS");
	case ROOMS: return std::string("ROOMS");
	case ROOM: return std::string("ROOM");
	case CREATE: return std::string("CREATE");
	case UPDATE: return std::string("UPDATE");
	case START: return std::string("START");
	case BACK: return std::string("BACK");
	case TEAM: return std::string("TEAM");
	case TEAMS: return std::string("TEAMS");
	case POSITIONING: return std::string("POSITIONING");
	case SPAWNS: return std::string("SPAWNS");
	case INIT_WORLD: return std::string("INIT_WORLD");
	case CREATE_OCCUPIER: return std::string("CREATE_OCCUPIER");
	case GOLD: return std::string("GOLD");
	case END: return std::string("END");
	default: break;
	}
	return std::string("UNDEFINED");
}
