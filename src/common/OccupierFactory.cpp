#include "common/OccupierFactory.hpp"
#include "common/Occupier.hpp"


Occupier* OccupierFactory::create_unit(sf::Vector2u pos, int team,
									   OccupierFactory::TYPE type)
{
	Occupier* p_occupier = nullptr;
	switch (type)
	{
	case OccupierFactory::TYPE::SOLDIER:
		p_occupier =
			new Occupier(pos, "Soldier", "spear", team, 30.f, 120.f, 3, 1);
		break;
	case OccupierFactory::TYPE::ELF:
		p_occupier = new Occupier(pos, "Elf", "knife", team, 20.f, 75.f, 7, 1);
		break;
	case OccupierFactory::TYPE::ORC:
		p_occupier = new Occupier(pos, "Orc", "sword", team, 50.f, 150.f, 2, 1);
		break;
	case OccupierFactory::TYPE::SCOUT:
		p_occupier =
			new Occupier(pos, "Scout", "bow", team, 20.f, 100.f, 4, 10);
		break;
	default: break;
	}
	return p_occupier;
}

int OccupierFactory::get_cost(OccupierFactory::TYPE type)
{
	switch (type)
	{
	case OccupierFactory::TYPE::SOLDIER: return 200;
	case OccupierFactory::TYPE::ELF: return 150;
	case OccupierFactory::TYPE::ORC: return 300;
	case OccupierFactory::TYPE::SCOUT: return 200;
	default: return 0;
	}
}

std::string OccupierFactory::get_unit_name(OccupierFactory::TYPE type)
{
	switch (type)
	{
	case OccupierFactory::TYPE::SOLDIER: return "Soldier";
	case OccupierFactory::TYPE::ELF: return "Elf";
	case OccupierFactory::TYPE::ORC: return "Orc";
	case OccupierFactory::TYPE::SCOUT: return "Scout";
	default: return "Unknown";
	}
}