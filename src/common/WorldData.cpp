/**
 * \file WorldData.cpp
 * \author Romain Mekarni
 */

#include <cassert>

#include "common/OccupierFactory.hpp"
#include "common/Query.hpp"
#include "common/WorldData.hpp"


int WorldData::distance(sf::Vector2u v1, sf::Vector2u v2)
{
	return (int)abs((int)v1.x - (int)v2.x) + abs((int)v1.y - (int)v2.y);
}

WorldData::WorldData(void)
{
	Log::write_debug("[WorldData]:\t Initialized a map of " +
					 std::to_string(WORLD_WIDTH) + "x" +
					 std::to_string(WORLD_HEIGHT) + " cells.");


	// Important
	for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
	{
		for (size_t x = 0ul; x < WORLD_WIDTH; ++x)
		{
			m_cells[y][x].x			 = x;
			m_cells[y][x].y			 = y;
			m_cells[y][x].m_occupier = nullptr;
		}
	}
	m_occupiers.clear();
	m_updated = true; // Force view to redraw
}

Query WorldData::update(Query in)
{
	sf::Int8 action;
	Query	query;
	while (in >> action)
	{
		switch (action)
		{
		case MOVE:
		{
			sf::Vector2u from, to;
			in >> from >> to;
			if (get_occupier(from)->is_alive())
			{
				Log::write_debug("[WorldData]:\t Moving " +
								 Log::to_string(from) + " to " +
								 Log::to_string(to));
				if (get_occupier(from)->go_to(to.x, to.y, *this))
					query << WORLD << get_cell(from);
			}
		}
		break;
		case ATTACK:
		{
			sf::Vector2u from, to;
			in >> from >> to;
			if (get_occupier(from)->is_alive() && get_occupier(to)->is_alive())
			{
				Log::write_debug("[WorldData]:\t Attacking from " +
								 Log::to_string(from) + " to " +
								 Log::to_string(to));

				Occupier* o_from = get_occupier(from);
				if ((WorldData::distance(from, to) <= o_from->get_range()) ||
					(o_from && o_from->go_to(to.x, to.y, *this, true)))
				{
					get_occupier(from)->attack(get_occupier(to));
					query << WORLD << get_cell(from) << get_cell(to);
				}
			}
		}
		break;

		case CREATE_OCCUPIER:
		{
			size_t				  x, y;
			int					  team;
			OccupierFactory::TYPE type;
			in >> x >> y >> team >> type;
			add_occupier(
				OccupierFactory::create_unit(sf::Vector2u(x, y), team, type));
			query << WORLD << get_cell(x, y);
		}
		break;
		default: break;
		}
	}
	return query;
}

void WorldData::update(float dt)
{
	for (auto& p_occupier : m_occupiers)
	{
		p_occupier->update(dt, *this);
	}
}

void WorldData::update(void)
{
	for (auto& p_occupier : m_occupiers)
		if (p_occupier) // Why can it be null ?
			p_occupier->update(*this);
}

void WorldData::add_occupier(Occupier* occupier)
{
	assert(occupier);
	Log::write_debug("[WorldData]:\t Added occupier.");
	set_occupier(occupier->get_position(), occupier);
	m_occupiers.emplace_back(occupier);
	m_updated = true; // Force view to redraw
}

Query WorldData::get_query(void)
{
	Query query;
	query << INIT_WORLD << *this;
	for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
		for (size_t x				= 0ul; x < WORLD_WIDTH; ++x)
			m_cells[y][x].m_updated = false;
	return query;
}

const Cell& WorldData::get_cell(size_t x, size_t y) const
{
	// assert(x <= WORLD_WIDTH && y <= WORLD_HEIGHT); // Borring
	if (x < WORLD_WIDTH && y < WORLD_HEIGHT)
		return m_cells[y][x];
	return m_cells[std::min(y, WORLD_HEIGHT - 1)][std::min(x, WORLD_WIDTH - 1)];
}

Cell& WorldData::get_cell(size_t x, size_t y)
{
	// assert(x <= WORLD_WIDTH && y <= WORLD_HEIGHT); // Borring
	if (x < WORLD_WIDTH && y < WORLD_HEIGHT)
		return m_cells[y][x];
	return m_cells[std::min(y, WORLD_HEIGHT - 1)][std::min(x, WORLD_WIDTH - 1)];
}

std::vector< const Cell* > WorldData::get_neighboor_cells(size_t x,
														  size_t y) const
{
	std::vector< const Cell* > res;
	if (x > 0)
		res.push_back(&get_cell(x - 1, y));
	if (x < WORLD_WIDTH - 1)
		res.push_back(&get_cell(x + 1, y));
	if (y > 0)
		res.push_back(&get_cell(x, y - 1));
	if (y < WORLD_HEIGHT - 1)
		res.push_back(&get_cell(x, y + 1));

	return std::move(res);
}

void WorldData::reinitialize()
{
	for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
	{
		for (size_t x = 0ul; x < WORLD_WIDTH; ++x)
		{
			m_cells[y][x].x			 = x;
			m_cells[y][x].y			 = y;
			m_cells[y][x].m_type	 = EMPTY;
			m_cells[y][x].m_occupier = nullptr;
		}
	}
	m_occupiers.clear();
	m_updated = true; // Force view to redraw
}

void WorldData::make_perlin(const Perlin& perlin)
{
	for (size_t y = 0ul; y < WORLD_HEIGHT; ++y)
	{
		for (size_t x = 0ul; x < WORLD_WIDTH; ++x)
		{
			// Chose cell from perlin value
			float cell_value = perlin.get(x, y) / 100.0;
			if (cell_value <= 0.1)
				m_cells[y][x].m_type = WATER;
			else if (cell_value <= 0.85)
				m_cells[y][x].m_type = EMPTY;
			else if (cell_value <= 0.94)
				m_cells[y][x].m_type = TREE;
			else if (cell_value <= 0.95)
				m_cells[y][x].m_type = HOUSE;
			else
				m_cells[y][x].m_type = ROCK;
		}
	}
}

void WorldData::remove_occupier(Occupier* occupier)
{
	set_occupier(occupier->get_position(), nullptr);
	get_cell(occupier->get_position()).m_updated = true;

	// std::remove_if(m_occupiers.begin(), m_occupiers.end(),
	// 			   [occupier](std::shared_ptr< Occupier > po) {
	// 				   return po.get() == occupier;
	// 			   });
	for (auto it = m_occupiers.begin(); it != m_occupiers.end(); it++)
	{
		if ((*it).get() == occupier)
		{
			m_occupiers.erase(it);
			break;
		}
	}
	m_updated = true; // Force view to redraw
}

void WorldData::new_turn(void)
{
	for (auto& occupier : m_occupiers)
	{
		occupier->new_turn();
	}
}

void WorldData::set_cell(size_t x, size_t y, TYPE type)
{
	assert(x < WORLD_WIDTH);
	assert(y < WORLD_HEIGHT);

	m_cells[y][x].m_type = type;
}

int WorldData::has_empty_team(size_t nb_team) const
{
	std::vector< int > teams(nb_team);

	// Count occupiers
	for (auto& p_occupier : m_occupiers)
		if (p_occupier->is_alive())
			teams[p_occupier->get_team()]++;

	// Find if empty one
	int winner = -1;
	for (int i = 0; i < (int)nb_team; i++)
		if (teams[i] > 0)
		{
			if (winner != -1)
				return -1;
			winner = i;
		}
	return winner;
}

WorldData::~WorldData()
{
}
