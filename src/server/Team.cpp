#include "server/Team.hpp"
#include "common/Query.hpp"
#include "server/Client.hpp"


Team::Team(std::string name)
	: m_name(name)
{
}

void Team::initialize(void)
{
	m_current_player = std::rand() % mvp_clients.size();
}

void Team::update(Query query, Client* client)
{
}

void Team::next_player(void)
{
	m_current_player++;
	if (m_current_player == mvp_clients.size())
		m_current_player = 0;
}
