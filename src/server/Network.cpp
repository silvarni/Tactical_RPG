#include <algorithm>

#include "server/Client.hpp"
#include "server/Network.hpp"


void Network::add_client(Client* client)
{
	mvp_clients.push_back(client);
}

void Network::remove_client(Client* client)
{
	mvp_clients.erase(
		std::remove(mvp_clients.begin(), mvp_clients.end(), client));
}

void Network::broadcast(Query query) const
{
	for (auto& client : mvp_clients)
		client->send(query);
}

void Network::broadcast(sf::Int8 in) const
{
	Query query;
	query << in;
	broadcast(query);
}

void Network::broadcast(std::string message) const
{
	Query query;
	query << CHAT << message;
	broadcast(query);
}

Query Network::dump_clients(void) const
{
	Query query;
	query << CLIENTS;
	for (auto client : mvp_clients)
		query << client->get_name() +
					 ((client->get_room())
						  ? " - " + client->get_room()->get_name()
						  : "");
	return query;
}
