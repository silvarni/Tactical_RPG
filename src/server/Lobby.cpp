#include "server/Lobby.hpp"
#include "common/Query.hpp"
#include "server/Client.hpp"

#include <regex>


void Lobby::add_client(Client* client)
{
	Network::add_client(client);
	Query query;
	query << NAME << client->get_name();
	client->send(query);
	client->send(dump_rooms());
	broadcast(dump_clients());
}

void Lobby::remove_client(Client* client)
{
	if (client->get_room())
		client->get_room()->remove_client(client);
	Network::remove_client(client);
	broadcast(dump_clients());
}

void Lobby::broadcast(Query query) const
{
	for (auto& client : mvp_clients)
		if (!client->get_room())
			client->send(query);
}

void Lobby::if_empty_then_remove(Room* room)
{
	if (!room->get_nb_clients())
	{
		for (size_t i = 0; i < mvu_rooms.size(); i++)
			if (!mvu_rooms[i]->get_nb_clients())
			{
				mvu_rooms[i].reset();
				mvu_rooms.erase(mvu_rooms.begin() + i);
				broadcast(dump_rooms());
				broadcast(dump_clients());
				break;
			}
	}
}

float Lobby::update(float dt)
{
	float next_turn		= 0;
	float min_next_turn = 10.f;
	for (auto& u_room : mvu_rooms)
	{
		next_turn = u_room->update(dt);
		if (next_turn < min_next_turn)
			min_next_turn = next_turn;
	}
	return min_next_turn;
}

void Lobby::update(Query query, Client* client)
{
	sf::Int8 action;
	Query	copy = query;
	query >> action;
	switch (action)
	{
	case CREATE:
	{
		std::string name;
		sf::Int8	nb_teams;
		sf::Int8	nb_players;
		sf::Int8	turn_time;
		sf::Int16   gold;
		query >> name >> nb_teams >> nb_players >> turn_time >> gold;
		name = std::regex_replace(name, std::regex("^\\s+|\\s+$"), "");

		bool already_used = false;
		for (auto& u_room : mvu_rooms)
			if (!u_room->get_name().compare(name))
			{
				already_used = true;
				break;
			}
		if (already_used || name == "")
		{
			client->send("[server] Invalid name of room, or already used.");
			return;
		}

		mvu_rooms.emplace_back(new Room(name, (int)nb_teams, (int)nb_players,
										(int)turn_time, (int)gold));
		Query out;
		out << JOIN << name << turn_time;
		client->send(out);
		mvu_rooms.back()->add_client(client);
		broadcast(dump_rooms());
		broadcast(dump_clients());
		return;
	}
	case ROOM:
	{
		sf::Int8 room;
		query >> room;
		Query out;
		out << ROOM << mvu_rooms[room]->get_name()
			<< (sf::Int8)mvu_rooms[room]->get_nb_teams()
			<< (sf::Int8)mvu_rooms[room]->get_nb_players()
			<< (sf::Int8)mvu_rooms[room]->get_turn_time();
		client->send(out);
		return;
	}
	case NAME:
	{
		std::string name;
		query >> name;
		name = std::regex_replace(name, std::regex("^\\s+|\\s+$"), "");
		bool already_used = false;
		for (auto client : mvp_clients)
			if (client->get_name() == name)
			{
				already_used = true;
				break;
			}
		if (already_used || name == "")
		{
			client->send("[server] Invalid name, or already used.");
			return;
		}
		client->set_name(name);
		Query out;
		out << NAME << client->get_name();
		client->send(out);
		broadcast(dump_clients());
		return;
	}
	default: break;
	}

	if (client->get_room())
	{
		Room* room = client->get_room();
		room->update(copy, client);
		if_empty_then_remove(room);
		if (action == BACK)
		{
			client->send(dump_rooms());
			client->send(dump_clients());
		}
	}
	else
	{
		switch (action)
		{
		case JOIN:
		{
			sf::Int8 room;
			query >> room;
			if (room >= (int)mvu_rooms.size())
				break;
			if (mvu_rooms[room]->full())
				client->send(FULL);
			else
			{
				Query out;
				out << JOIN << mvu_rooms[room]->get_name()
					<< (sf::Int8)mvu_rooms[room]->get_turn_time();
				client->send(out);
				mvu_rooms[room]->broadcast("[server] " + client->get_name() +
										   " entered the room.");
				mvu_rooms[room]->add_client(client);
				broadcast(dump_rooms());
				broadcast(dump_clients());
			}
			break;
		}
		case CHAT: broadcast(copy); break;
		default:
			Log::write_debug("[server] Action has not been treated /!\\");
			break;
		}
	}
}

Query Lobby::dump_rooms(void) const
{
	Query query;
	query << ROOMS;
	for (auto& u_room : mvu_rooms)
		query << u_room->get_name() + " - " +
					 std::to_string(u_room->get_nb_clients()) + "/" +
					 std::to_string(u_room->get_nb_players());
	return query;
}
