#include "server/Client.hpp"
#include "common/Query.hpp"


void Client::send(Query query)
{
	m_socket.send(query);
}

void Client::send(sf::Int8 in)
{
	Query query;
	query << in;
	send(query);
}

void Client::send(std::string message)
{
	Query query;
	query << CHAT << message;
	send(query);
}

Client::~Client()
{
}
