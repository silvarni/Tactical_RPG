#include "server/Room.hpp"
#include "common/OccupierFactory.hpp"
#include "common/Query.hpp"
#include "common/WorldData.hpp"
#include "server/Client.hpp"


Room::Room(std::string name, int nb_teams, int nb_players, int turn_time,
		   int gold)
	: Network()
	, m_name(name)
	, m_nb_players(nb_players)
	, m_perlin(WORLD_WIDTH, WORLD_HEIGHT, 5)
	, m_turn_time((float)turn_time)
	, m_initial_gold(gold)
{
	for (int i = 0; i < nb_teams; i++)
	{
		mv_teams.emplace_back("Team " + std::to_string(i));
		m_spawns.emplace_back();
	}

	reinitialize_game();
}

void Room::reinitialize_game(void)
{
	m_game_running = false;
	m_positioning  = false;
	for (auto& client : mvp_clients)
	{
		client->set_ready(false);
		client->set_positioned(false);
	}
}

void Room::initialize_map(void)
{
	// Generate it
	m_world.reinitialize();
	m_world.make_perlin(m_perlin);

	// Add spawn areas
	add_spawn_zone(0, sf::Vector2u(3, 10), sf::Vector2u(6, 20));
	add_spawn_zone(1, sf::Vector2u(23, 10), sf::Vector2u(26, 20));
}

void Room::initialize_game(void)
{
	broadcast(START);
	initialize_map();
	broadcast(m_world.get_query());

	// Chose random starting team
	std::srand(std::time(0));
	m_current_team = std::rand() % mv_teams.size();
	for (size_t i = 0; i < mv_teams.size(); i++)
	{
		mv_teams[i].initialize();
		Query out;

		// Spawns
		out << SPAWNS;
		for (auto& v : m_spawns[i])
			out << v;

		mv_teams[i].broadcast(out);
	}
	broadcast("[server] In tactic mode, choose and place your units in the "
			  "grey zone.");

	// Gold
	Query out;
	out << GOLD << (sf::Int16)m_initial_gold;
	for (size_t i = 0; i < mv_teams.size(); i++)
	{
		mv_teams[i].broadcast(out);
		for (auto& p_client : mv_teams[i].get_clients())
			p_client->set_gold(m_initial_gold);
	}

	m_elapsed_time = 0.f;
	m_positioning  = true;
	broadcast(turn(m_positioning_time));
}

void Room::start_game(void)
{
	m_game_running = true;

	auto occupiers = m_world.get_occupiers();
	for (auto occupier = occupiers.rbegin(); occupier != occupiers.rend();
		 occupier++)
	{
		bool is_in_spawn_zone = false;
		for (auto zone : m_spawns[(*occupier)->get_team()])
			if ((*occupier)->get_position() == zone)
				is_in_spawn_zone = true;
		if (!is_in_spawn_zone)
			m_world.remove_occupier(occupier->get());
		else
			(*occupier)->set_positioning(false);
	}

	broadcast(m_world.get_query());
}

void Room::add_client(Client* client)
{
	Network::add_client(client);
	client->set_room(this);

	sf::Uint8 team = 0;
	size_t	min  = mv_teams[0].get_clients().size();
	for (size_t i = 0; i < mv_teams.size(); i++)
		if (min > mv_teams[i].get_clients().size())
		{
			team = i;
			min  = mv_teams[i].get_clients().size();
		}

	client->set_team(&(mv_teams[team]));
	mv_teams[team].add_client(client);
	Query query;
	query << TEAM << team;
	client->send(query);
	broadcast(dump_teams());
}

void Room::remove_client(Client* client)
{
	if (m_game_running || m_positioning)
	{
		broadcast(BACK);
		reinitialize_game();
	}
	client->get_team()->remove_client(client);
	client->set_team(nullptr);
	client->set_room(nullptr);
	Network::remove_client(client);
	broadcast("[server] " + client->get_name() + " quit the party.");
	broadcast(dump_teams());
}

float Room::update(float dt)
{
	m_elapsed_time += dt;
	if (m_pause)
	{
		if (m_elapsed_time >= m_delay)
		{
			// End Management
			int winner_team = m_world.has_empty_team(mv_teams.size());
			if (winner_team != -1 && m_positioning == false)
			{
				reinitialize_game();
				Query out;
				out << END << (sf::Int8)winner_team;
				broadcast(out);
			}

			m_elapsed_time = 0.f;
			m_pause		   = false;
			m_world.new_turn();
			mv_teams[m_current_team].next_player();
			m_current_team++;
			if (m_current_team == mv_teams.size())
				m_current_team = 0;
			mv_teams[m_current_team].get_current_player()->send(
				turn(m_turn_time));
			return m_turn_time;
		}
		return m_delay - m_elapsed_time;
	}
	else if (m_game_running)
	{
		if (m_elapsed_time >= m_turn_time)
		{
			m_pause = true;
			mv_teams[m_current_team].get_current_player()->send(
				turn(m_turn_time));
			m_elapsed_time = 0.f;
			return m_delay;
		}
		return m_turn_time - m_elapsed_time;
	}
	else if (m_positioning)
	{
		if (m_elapsed_time >= m_positioning_time)
		{
			if (m_world.get_occupiers().size() == 0)
			{
				broadcast(BACK);
				reinitialize_game();
				broadcast("[server] There is no army on battlefield, so there "
						  "is no battle.");
				return 1;
			}
			broadcast(turn(m_turn_time));
			start_game();
			m_pause		   = true;
			m_elapsed_time = 0.f;
			m_positioning  = false;
			return m_delay;
		}
		return m_positioning_time - m_elapsed_time;
	}
	return 1; // osef positif
}

void Room::update(Query query, Client* client)
{
	sf::Int8 action;
	Query	copy = query;
	query >> action;
	switch (action)
	{
	case BACK:
		if (!(m_game_running || m_positioning))
			remove_client(client);
		else
		{
			broadcast(BACK);
			reinitialize_game();
			broadcast("[server] " + client->get_name() + " stop the party.");
		}
		break;
	case JOIN:
	{
		sf::Int8 team;
		query >> team;
		client->get_team()->remove_client(client);
		mv_teams[team].add_client(client);
		client->set_team(&mv_teams[team]);
		broadcast(dump_teams());
		break;
	}
	case READY:
	{
		client->toggle_ready();
		broadcast(dump_teams());
		bool team_balanced = true;
		for (auto& team : mv_teams)
			if (team.get_nb_clients() != m_nb_players / (int)mv_teams.size())
			{
				team_balanced = false;
				break;
			}
		bool all_ready = true;
		for (auto& client : mvp_clients)
			if (!(client->is_ready()))
			{
				all_ready = false;
				break;
			}
		if (all_ready)
		{
			if (team_balanced)
				initialize_game();
			else
				broadcast("[server] Teams unbalanced; not starting the game.");
		}
		break;
	}
	case POSITIONING:
	{
		if (m_game_running)
			m_elapsed_time = m_turn_time;
		else
		{
			client->toggle_positioned();
			bool all_positioned = true;
			for (auto& client : mvp_clients)
				if (!(client->is_positioned()))
				{
					all_positioned = false;
					break;
				}
			if (all_positioned)
				m_elapsed_time = m_positioning_time;
		}
		break;
	}
	case WORLD:
	{
		if (m_positioning ||
			client == mv_teams[m_current_team].get_current_player())
		{
			Query out = m_world.update(query);
			if (out)
			{
				broadcast(out);
				out >> action;
				out >> m_world;
				m_world.update();
			}
		}
		break;
	}
	case CREATE_OCCUPIER:
	{
		Log::write_debug("[server]:\t Managing create_occupier message.");
		// Security
		if (m_positioning)
		{
			OccupierFactory::TYPE type;
			int					  x, y;
			int					  team;
			query >> x >> y >> team >> type;

			// Verify gold
			int cost = OccupierFactory::get_cost(type);
			if (client->get_gold() >= cost)
			{
				Log::write_debug("[Room]:\t Client " + client->get_name() +
								 " creates unit type " + std::to_string(type) +
								 " for " + std::to_string(cost) + " gold.");
				client->pay(cost);
				m_world.add_occupier(OccupierFactory::create_unit(
					sf::Vector2u(x, y), team, type));
				Query out;
				out << WORLD << m_world.get_cell(x, y);
				broadcast(out);
				out.clear();
				out << GOLD << (sf::Int16)client->get_gold();
				client->send(out);
				break;
			}
			else
				client->send(
					"[server] You don't have enough money to buy this unit.");
		}
	}
	break;
	case CHAT: broadcast(copy); break;
	default:
		Log::write_debug("[server]\t Action has not been treated /!\\");
		break;
	}
}

Query Room::turn(int turn_time)
{
	Query query;
	query << TURN << (sf::Int8)turn_time;
	return query;
}

Query Room::dump_teams(void) const
{
	Query query;
	query << TEAMS;
	for (auto& team : mv_teams)
	{
		query << team.get_name() << (sf::Int8)team.get_clients().size();
		for (auto& client : team.get_clients())
			query << client->get_name() + " - " +
						 ((client->is_ready()) ? "ready" : "not ready");
	}
	return query;
}

void Room::add_spawn_zone(int team, sf::Vector2u pos1, sf::Vector2u pos2)
{
	for (size_t i = pos1.x; i < pos2.x; i++)
		for (size_t j = pos1.y; j < pos2.y; j++)
		{
			m_spawns[team].emplace_back(i, j);
			m_world.set_cell(i, j, TYPE::EMPTY);
		}
}
