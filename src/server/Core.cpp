/**
 * \file Core.cpp
 */


#include "server/Core.hpp"
#include "common/Log.hpp"
#include "common/Query.hpp"
#include "server/Client.hpp"


sig_atomic_t Core::m_running = 1;


Core::Core(size_t port)
	: m_port(port)
	, hand_SIGINT(signal(SIGINT, Core::SIGINT_handler))
{
	Log::initialize("server");
	Log::write_debug("[Core]:\tServer log initialized.");
	if (m_listener.listen(port) != sf::Socket::Done)
	{
		Log::write_error("[Core]:\t Error while listening at port " +
						 std::to_string(port) + " (Is it busy ?)");
		exit(EXIT_FAILURE);
	}
	m_selector.add(m_listener);
	mvp_clients.push_back(new Client());
}

void Core::SIGINT_handler(int param)
{
	m_running = false;
}

void Core::receive(void)
{
	m_last = std::chrono::steady_clock::now();
	while (m_running)
	{
		std::chrono::time_point< std::chrono::steady_clock > now =
			std::chrono::steady_clock::now();
		std::chrono::duration< double > delta = now - m_last;
		m_last								  = now;
		m_next_turn							  = m_lobby.update(delta.count());

		if (m_selector.wait(sf::seconds(m_next_turn)))
		{
			if (m_selector.isReady(m_listener))
			{
				switch (m_listener.accept(mvp_clients.back()->get_socket()))
				{
				case sf::Socket::Done:
				{
					std::string name =
						"Player" + std::to_string(mvp_clients.size());
					bool already_used = true;
					while (already_used)
					{
						already_used = false;
						for (auto client : mvp_clients)
							if (!client->get_name().compare(name))
							{
								name += "_";
								already_used = true;
								break;
							}
					}
					mvp_clients.back()->set_name(name);
					Log::write_debug("[Core]:\tClient " +
									 mvp_clients.back()->get_name() +
									 " added.");
					m_selector.add(mvp_clients.back()->get_socket());
					m_lobby.add_client(mvp_clients.back());
					mvp_clients.push_back(new Client());
					break;
				}
				case sf::Socket::NotReady: break;
				case sf::Socket::Partial: break;
				case sf::Socket::Disconnected:
				case sf::Socket::Error:
					Log::write_error(
						"[Core]:\tError while accepting new client on port " +
						std::to_string(m_port));
				}
			}
			for (auto client : mvp_clients)
			{
				if (m_selector.isReady(client->get_socket()))
				{
					Query query;
					switch (client->get_socket().receive(query))
					{
					case sf::Socket::Done: update(query, client); break;
					case sf::Socket::NotReady: break;
					case sf::Socket::Partial: break;
					case sf::Socket::Disconnected:
						Log::write_error("[Core]:\t Client " +
										 client->get_name() +
										 " is unreachable. Removing it.");
						remove_client(client);
						break;
					default: break;
					}
				}
			}
		}
	}
}

void Core::update(Query query, Client* client)
{
	sf::Int8 action;
	Query	copy = query;
	query >> action;
	Log::write_debug("[Core]\t" + client->get_name() + " -> " +
					 Query::log(action));
	switch (action)
	{
	case DISCONNECT: remove_client(client); break;
	default: m_lobby.update(copy, client); break;
	}
}

void Core::remove_client(Client* client)
{
	client->send(DISCONNECT);
	Room* room = client->get_room();
	if (room)
	{
		room->remove_client(client);
		m_lobby.if_empty_then_remove(room);
	}
	m_lobby.remove_client(client);
	m_selector.remove(client->get_socket());
	Network::remove_client(client);
	Log::write_debug("[Lobby]\t Client" + client->get_name() +
					 " disconnected. Removing it.");
	delete client;
	// if (mvp_clients.size() == 1)
	// 	m_running = 0;
}

Core::~Core(void)
{
	m_running = 0;

	delete mvp_clients.back();
	mvp_clients.erase(mvp_clients.end() - 1);
	while (mvp_clients.size())
		remove_client(mvp_clients.back());

	m_selector.remove(m_listener);
	m_listener.close();
	Log::write_debug("[Core]:\t Server shut down.");
	Log::destroy();
}
