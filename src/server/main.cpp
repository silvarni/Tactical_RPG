#include "server/Core.hpp"
#include <iostream>

void usage(void)
{
	std::cerr << "usage: server [port]" << std::endl;
}

int main(const int argc, const char** argv)
{
	size_t port = 53000;
	if (argc == 1)
	{
		std::cerr << "No port given; use 53000 as default." << std::endl;
	}
	else if (argc == 2)
	{
		port = std::atoi(argv[1]);
		if (port == 0)
		{
			std::cerr << "Given argument is not a number !" << std::endl;
			usage();
			return 2;
		}
		std::cerr << "Using port " << port << "." << std::endl;
	}
	else
	{
		std::cerr << "Too many arguments ! ";
		usage();
		return 1;
	}

	Core core(port);
	core.receive();
	return 0;
}
