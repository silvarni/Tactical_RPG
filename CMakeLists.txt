cmake_minimum_required(VERSION 3.6)
project(Tactical_RPG)

include(CMakeToolsHelpers OPTIONAL)

set(EXECUTABLE_OUTPUT_PATH "bin/")
include_directories("${CMAKE_SOURCE_DIR}/include")

add_definitions(-Wall -std=c++17)

find_package(Threads REQUIRED)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/Modules")

find_package(SFML 2.4 COMPONENTS system window graphics network audio REQUIRED)
find_package(OpenGL REQUIRED)

include_directories(${SFML_INCLUDE_DIR})
include_directories("${CMAKE_SOURCE_DIR}/include/client/imgui")
include_directories("${CMAKE_SOURCE_DIR}/include/client")

find_package(Doxygen)
if (DOXYGEN_FOUND)
    add_subdirectory(doc)
endif()

file(GLOB SRC_SRV "src/server/*.cpp")
file(GLOB SRC_CLT "src/client/*.cpp")

file(GLOB SRC_CMN "src/common/*.cpp")
file(GLOB_RECURSE INCLUDE "include")

add_executable(server ${SRC_SRV} ${SRC_CMN} ${INCLUDE})
add_executable(client ${SRC_CLT} ${SRC_CMN} ${INCLUDE} ${IMGUI_SOURCES} ${IMGUI_SFML_SOURCES} )

target_link_libraries(server ${SFML_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(client ${IMGUI_SFML_DEPENDENCIES} ${SFML_LIBRARIES}  ${OPENGL_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

# add_subdirectory(report)

add_custom_target(game)
add_dependencies(game server client)

install(TARGETS client RUNTIME DESTINATION bin)
install(DIRECTORY assets DESTINATION .)
if (WIN32)
    install(DIRECTORY Modules/bin DESTINATION .)
endif()

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
set(CPACK_PACKAGE_INSTALL_DIRECTORY "Silvarni_ArtOfWar")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENCE.TXT")
set(CPACK_RESOURCE_FILE_README "${CMAKE_SOURCE_DIR}/README.md")

include(CPack)
