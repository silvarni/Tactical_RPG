# Tactical RPG

## Project Description
A tactical RPG is a game where you have to control a set of characters with various skills and stats on a fight area against an ennemy.
This kind of game is a mix between the chess game and a turn based strategy game.
The objective of this project is to produce an Online multiplayer Tactical RPG.


## What is asked
Using the SFML (Simple and fast Multimedia Library), it is about producing a network based RPG.
Once familiarized with the game programming, it needs to be playable by two players online.
The game is composed of a game server and a client allowing to connect to it and launch a game.
The whole application is made with C++ and was presented as the most successful project born from the University.

## Tutoring
The project is tutored by Julien Bernard, PHD and teacher at the Franche-Comté University.

[![vidéo démo](https://img.youtube.com/vi/15qQMjNMx3w/0.jpg)](https://www.youtube.com/watch?v=15qQMjNMx3w)

------

## Dependencies
- CMake >= 3.8
- SFML :
    - sfml-graphics >= 2.4
    - sfml-audio >= 2.4
    - sfml-network >= 2.4
- OpenGL

## Coding Standard
- Use the **snake_case** 
- **Constants** are UPPERCASE\_WITH\_UNDERSCORE : ```#define BUFFER_SIZE 1048```
- All the code is GPL2, and all the headers *must contain the licence terms*
- Prefixes
    - **Pointers** are prefixed by p_ : ```p_character```
    - **Smart pointers** are prefixed with first letter : ```std::shared_ptr<Object> sp_name;``` 
    - **Member variables** are absolute prefixed with 'm' : ``` std::string* mp_name;```
- Suffixes  
    - **Arrays** are suffixed by 's' : ```std::vector<Character*> characters;```

## How to

```bash
mkdir build && cd build
cmake ..
make
cd ..
./build/bin/server
./build/bin/client
```

## See report/report.pdf